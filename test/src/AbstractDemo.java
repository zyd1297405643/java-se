public abstract class AbstractDemo {
    // 普通成员变量
    int a;

    //普通成员方法
    void c() {
    }

    //抽象方法
    abstract void a();

    //构造方法
    public AbstractDemo() {
    }

    public static void m() {
    }

}
