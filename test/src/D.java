public interface D {
    // 接口中定义变量 默认加上了public static final 来修饰
    public static final int a = 10;

    //静态方法 jdk1.8
    static void m() {
    }

    //默认方法
    default void m1() {
    }
    //私有方法
    private void m2() {
    }
}
