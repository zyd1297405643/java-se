import java.util.Scanner;
import java.util.regex.Pattern;

/*写⼀个函数，将⼀个字符串中的单词反转过来，单词的定义是：完全由字⺟组成且由空
格分开的字符串。例如下⾯的字符串:“ a hello1 abc good!”，其中“a”和“abc”是单词，
⽽“hello1”和“good!”不是单词（原因是这两个字符串中包含⾮字⺟的字符1和!）。
反转过来的结果就是“ a hello1 cba good!”*/
public class ReverDemo {
    public static void main(String[] args) {
        System.out.println("输入字符串");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String[] strings = s.split(" ");
        String reg = "[a-zA-Z]+";
        for (String word : strings) {
            if (Pattern.matches(reg, word)) {

            } else {
            }
        }
    }
}


