import java.util.Scanner;

public class Demo {
    public static void main(String args[]) {
   
        System.out.println("输入英文字符串");
        Scanner console = new Scanner(System.in);
        String s = console.nextLine();
        int[] count = new int[55];
        for (int i = 0; i <s.length(); i++) {
            char ch = s.charAt(i);
            if ((ch >= 65 && ch <= 90 )|| (ch >= 97 && ch <= 122)) {
                int index = ch - 65;
                count[index]+= 1;
            }

        }
        for(int j=0; j<count.length; j++){
            if(count[j]!=0)
                System.out.println("字母"+(char)(j+65)+"出现次数："+count[j]);
        }
    }
}
