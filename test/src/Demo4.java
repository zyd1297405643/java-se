

public class Demo4 {
    public static void main(String[] args) {
        System.out.println("main");
        new B();

    }
}

class A {
    static {
        System.out.println("A.静态代码块");
    }
    {  System.out.println("A.非静态代码块");
    }
    public A() {
        System.out.println("A 无参构造函数");
    }
}

class B {
    static {
        System.out.println("B.静态代码块");
    }
    {  System.out.println("B.非静态代码块");
    }
    public B() {
        System.out.println("B 无参构造器");
    }


}