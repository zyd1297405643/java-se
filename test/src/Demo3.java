import java.util.Map;

public class Demo3 {
    public static void main(String[] args) {

        //强转 大类型转成成小类型
        double a=10;
        int b= (int) a;
        System.out.println(b);

        int c=10;
        double d=c;
        System.out.println(d);
    }
}
