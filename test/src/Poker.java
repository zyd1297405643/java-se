import java.util.ArrayList;
import java.util.Collections;

public class Poker {
    public static void main(String[] args) {
        //集合pokerBox 存放纸牌
        ArrayList<String> pokerBox = new ArrayList();
        //集合number 存放数字
        ArrayList<String> numbers = new ArrayList();
        //集合color 存放花色
        ArrayList<String> colors = new ArrayList();
        // 添加数字
        for (int i = 2; i <=10; i++) {
            numbers.add(i + "");
        }
        numbers.add("J");
        numbers.add("Q");
        numbers.add("K");
        numbers.add("A");

        //添加花色
        colors.add("♥");
        colors.add("♦");
        colors.add("♠");
        colors.add("♣");

        //将color + number 添加到 pokerBox集合
        for (String color : colors) {
            for (String number : numbers) {
                String pokers = color + number;
                pokerBox.add(pokers);
            }
        }
        pokerBox.add("⼩☺");
        pokerBox.add("⼤☠");

        // 洗牌 是不是就是将 牌盒中 牌的索引打乱
        // Collections类 ⼯具类 都是 静态⽅法
        // shuffer⽅法
        /*
         * static void shuffle(List<?> list)
         * 使⽤默认随机源对指定列表进⾏置换。*/
        Collections.shuffle(pokerBox);

        //创建三个 玩家 player 集合， 一个底牌的集合 存放地主牌
        ArrayList<String> player1 = new ArrayList<>();
        ArrayList<String> player2 = new ArrayList<>();
        ArrayList<String> player3 = new ArrayList<>();
        ArrayList<String> dipai = new ArrayList<>();

        for (int i = 0; i < pokerBox.size(); i++) {
            String card = pokerBox.get(i);
            if (i >= 51) {
                dipai.add(card);
            } else {
                if (i % 3 == 0) {
                    player1.add(card);
                } else if (i % 3 == 1) {
                    player2.add(card);
                } else {
                    player3.add(card);
                }
            }
        }
        System.out.println(player1);
        System.out.println(player2);
        System.out.println(player3);
        System.out.println(dipai);
    }

}
