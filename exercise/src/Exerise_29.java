import java.util.Arrays;

/*求一个3*3矩阵对角线元素之和*/
public class Exerise_29 {
    public static void main(String[] args) {
        int matrix[][]=new int[3][3];
        int sum = 0;
        System.out.println("随机生成3*3的矩阵");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j]=(int)(Math.random()*10);

                System.out.print(matrix[i][j]+"\t");
                if(j==2)
                    System.out.println();
            }

        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == j || (i == matrix.length - 1 && j == 0) || (i == 0 && j == matrix.length - 1))
                    sum = sum + matrix[i][j];
            }
        }
        System.out.println("矩阵对角线元素之和"+sum);


    }
}
