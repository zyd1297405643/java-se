import java.util.Scanner;

/*利用条件运算符的嵌套来完成此题：
学习成绩> =90分的同学用A表示，60-89分之间的用B表示，60分以下的用C表示。 */
public class Exerise_5_ifcase {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        System.out.println("输入一个0~100的整数");
        int nummber=sc.nextInt();
        if(nummber>=90)
            System.out.println("A");
        else if(nummber>=60&&nummber<=89)
            System.out.println("B");
        else
            System.out.println("C");
    }
}
