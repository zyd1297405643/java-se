import java.util.Scanner;

/*输入三个整数x,y,z，请把这三个数由小到大输出。*/
public class Exerise_15 {
    public static void main(String[] args) {
        int x, y, z;
        Scanner sc = new Scanner(System.in);
        System.out.println("输入三个整数x,y,z");
        x = sc.nextInt();
        y = sc.nextInt();
        z = sc.nextInt();
        if (x > y) {
            int temp = x;
            x = y;
            y = temp;
        }
        if (x > z) {
            int temp = x;
            x = z;
            z = temp;
        }
        if (y > z) {
            int temp=y;
            y=z;
            z=temp;
        }
        System.out.println(x+","+y+","+z);
    }
}
