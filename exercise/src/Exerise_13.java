

import java.util.Scanner;

/*一个整数，它加上100后是一个完全平方数，再加上168又是一个完全平方数，请问该数是多少？
PS: 完全平方数，及其平方根为整数的数
*/
public class Exerise_13 {
    public static void main(String[] args) {
        int a;
        int b;
        for (int i = 0; i < 10000; i++) {
            a = (int) Math.sqrt(i + 100);
            b = (int) Math.sqrt(i + 268);
            if (a * a == i + 100 && b * b == i + 268)
                System.out.println(i);
        }

    }
}
