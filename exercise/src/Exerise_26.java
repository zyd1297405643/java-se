
import java.util.Scanner;
/*请输入星期几的第一个字母来判断一下是星期几，如果第一个字母一样，则继续判断第二个字母。   */
public class Exerise_26 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str;
        System.out.println("输入第一个字母");
        str = sc.nextLine();
        if ("m".equals(str)) {
            System.out.println("这天星期一");
        } else if ("t".equals(str)) {
            System.out.println("无法判断星期几，请输入第二个字母");
            str = sc.nextLine();
            if ("u".equals(str)) {
                System.out.println("这天星期二");
            } else if ("h".equals(str)) {
                System.out.println("这天星期四");
            }
        } else if ("w".equals(str)) {
            System.out.println("这天是星期三");

        } else if ("f".equals(str)) {
            System.out.println("这天是星期五");
        } else if ("s".equals(str)) {
            System.out.println("无法判断星期几，请输入第二个字母");
            str = sc.nextLine();
            if ("a".equals(str)) {
                System.out.println("这天是星期六");
            } else if ("u".equals(str)) {
                System.out.println("这天是星期日");
            }
        }
    }
}
