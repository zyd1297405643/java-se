/*题目：打印出所有的 "水仙花数 "，
所谓 "水仙花数 "是指一个三位数，其各位数字立方和等于该数本身。
例如：153是一个 "水仙花数 "，因为153=1的三次方＋5的三次方＋3的三次方。*/
public class Exerise_3_Narcissus {
    public static void main(String[] args) {
        for (int i = 100; i <= 999; i++) {
            double a=i/100;//百位
            double b=i/10%10;//十位
            double c=i%10;//个位
            //Math.pow(底数,幂)
            if(i==(int) Math.pow(a,3)+(int)Math.pow(b,3)+(int)Math.pow(c,3))
                System.out.println(i+"是水仙花数");

        }
    }
}
