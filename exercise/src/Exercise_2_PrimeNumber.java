/*题目：判断101-200之间有多少个素数，并输出所有素数。
程序分析：判断素数的方法：用一个数分别去除2到sqrt(这个数)，
如果能被整除， 则表明此数不是素数，反之是素数。
*/
public class Exercise_2_PrimeNumber {
    public static void main(String[] args) {
        int j;
        for (int i = 101; i <= 200; i++) {
            int flag = 1;
            int k = (int) Math.sqrt(i);
            for (j = 2; j <=k; j++)
            //如果i%j==0,说明i的因子不止1和他本身。
            {
                if (i % j == 0) {
                    flag = 0;
                    break;
                }
            }
            if (flag == 1) {
                System.out.println(i + "是素数 ");
            }



        }

    }
}
