
/*给一个不多于5位的正整数，要求：一、求它是几位数，二、逆序打印出各位数字*/

import java.util.Scanner;
public class Exerise_24 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int num= s.nextInt();
        if(num< 0 || num > 10000) {
            System.out.println("Error Input, please run this program Again");
            System.exit(0);
        }
        if(num >=0 && num<=9) {
            System.out.println( num + "是一位数");
            System.out.println("按逆序输出是" );
            method(num);
        } else if(num >= 10 && num <= 99) {
            System.out.println(num+ "是二位数");
            System.out.println("按逆序输出是" );
           method(num);
        } else if(num >= 100 && num <= 999) {
            System.out.println(num + "是三位数");
            System.out.println("按逆序输出是" );
         method(num);
        } else if(num >= 1000 &&num <= 9999) {
            System.out.println(num + "是四位数");
            System.out.println("按逆序输出是" );
           method(num);
        }
    }

    public static void method(int num) {
        String str = Integer.toString(num);//数字转化成字符串
        char chars[];
        chars = str.toCharArray();//字符串转化串字符
        for (int i = chars.length - 1; i >= 0; i--)
            System.out.print(chars[i]);
    }
}
