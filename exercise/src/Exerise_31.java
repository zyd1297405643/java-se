import java.util.Arrays;

/*将一个数组逆序输出。   */
public class Exerise_31 {
    public static void main(String[] args) {
        int arrays[] = new int[10];
        int arrays1[] = new int[10];
        for (int i = 0; i < arrays.length; i++) {

            arrays[i] = (int) (Math.random() * 100);

        }

        System.out.println("原数组为：" + Arrays.toString(arrays));
         //复制数组 System.arraycopy(原数组,开始复制的位置,复制的数组,开始复制的位置,复制的长度);
        System.arraycopy(arrays,0,arrays1,0,10);
        System.out.print("逆序输出为:");
        for (int i = arrays.length - 1; i >= 0; i--) {
            System.out.print(arrays1[i]+"\t");
        }

    }
}
