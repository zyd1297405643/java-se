import java.util.Scanner;

/*有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），
凡报到3的人退出圈子，问最后留下的是原来第几号的那位。 */
public class Exerise_37 {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.println("输入围成圈的人数");
        n = sc.nextInt();
        boolean array[] = new boolean[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = true;
        }
        System.out.println("最后留下来的是"+method(array)+"号");
    }

    public static int method(boolean array[]) {
        int l = array.length;
        int count = 0;
        int index = 0;
        while (l > 1) {
            for (int i = 0; i < array.length; i++) {
                if (array[i]) {
                    count++;
                    if (count == 3) {
                        count = 0;
                        array[i] = false;
                        l--;
                    }
                }
            }
        }
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i] + " ");
            if (array[i]) {
                index += i+1;
            }
        }
        return index;
    }


}
