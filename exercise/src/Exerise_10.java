import java.util.Scanner;

/*一球从100米高度自由落下，每次落地后反跳回原高度的一半,再落下，
求它在第10次落地时，共经过多少米？第10次反弹多高？ */
public class Exerise_10 {
    public static void main(String[] args) {
        double high=100;
        int n;
        double sum=0;
        Scanner sc=new Scanner(System.in);
        System.out.println("求第几次反弹高度");
        n=sc.nextInt();
        for (int i = 0; i <n; i++) {
            sum=  sum+high+high/2;
            high=high/2;
        }
        System.out.println(n+"次落地后经过"+sum+"米");
        System.out.println(n+"第十次落地"+high);
    }
}
