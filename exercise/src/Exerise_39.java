import java.util.Scanner;

/*编写一个函数，输入n为偶数时，调用函数求1/2+1/4+...+1/n,
当输入n为奇数时，调用函数1/1+1/3+...+1/n(利用指针函数)*/
public class Exerise_39 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入一个整数n");
        int n = sc.nextInt();
        System.out.println("结果为:" + method(n));

    }

    public static double method(int n) {
        double sum = 0;
        if (n % 2 == 0) {
            for (int i = 2; i <= n; i = i + 2) {
                sum = sum + (double) 1 / i;
            }
        } else {
            for (int i = 1; i <= n; i = i + 2) {
                sum = sum + (double) 1 / i;
            }
        }
        return sum;
    }
}
