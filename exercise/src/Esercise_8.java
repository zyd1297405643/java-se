import java.util.Scanner;

//求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个数字。
// 例如2+22+222+2222+22222(此时共有5个数相加)，几个数相加有键盘控制。
public class Esercise_8 {
    public static void main(String[] args) {
int sum=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("输入一个0~9数字");
        int num = sc.nextInt();
        System.out.println("需要多少个数相加");
        int size = sc.nextInt();
        int temp=num;
        for (int i = 0; i < size; i++) {
            sum+=temp;
            temp=temp*10+num;
        }
        System.out.println(sum);

    }


}

