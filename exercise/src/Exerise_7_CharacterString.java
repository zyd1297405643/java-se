//输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。
import java.util.Scanner;

public class Exerise_7_CharacterString {
    public static void main(String[] args) {
        int letter_Count = 0;
        int nummber_Connt = 0;
        int space_Count = 0;
        int other_Count = 0;
        char chars[];
        String str;
        Scanner sc = new Scanner(System.in);
        System.out.println("输入字符串");
        //next()不会吸取字符前/后的空格/Tab键，只吸取字符，开始吸取字符（字符前后不算）直到遇到空格/Tab键/回车截止吸取；
        //　nextLine()吸取字符前后的空格/Tab键，回车键截止。
       str = sc.nextLine();
        //toCharArray() 将String类型 转换成char类型
        chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= '0' && chars[i] <= '9')
                nummber_Connt++;
            else if (chars[i] >= 'a' && chars[i] <= 'z' || chars[i] >= 'A' && chars[i] <= 'Z')
                letter_Count++;
            else if (chars[i] == ' ')
                space_Count++;
            else
                other_Count++;
        }
        System.out.println("英文字母个数" + letter_Count);
        System.out.println("数字个数" + nummber_Connt);
        System.out.println("空格个数" + space_Count);
        System.out.println("其他字符个数" + other_Count);
    }
}


