import java.util.Scanner;

/*输入3个数a,b,c，按大小顺序输出。  */
public class Exerise_34 {
    public static void main(String[] args) {
        int a, b, c;
        Scanner sc = new Scanner(System.in);
        System.out.println("输入3个数");
        a = sc.nextInt();
        b = sc.nextInt();
        c = sc.nextInt();
        if (a > b) {
            int temp = a;
            a = b;
            b = temp;
        }
        if (a > c) {
            int temp=a;
            a=c;
            c=temp;
        }
        if (b > c) {
            int temp = b;
            b = c;
            c = temp;
        }

        System.out.println(a+" "+b+" "+c);
    }
}
