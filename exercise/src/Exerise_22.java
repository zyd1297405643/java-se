/*利用递归方法求5!。*/
public class Exerise_22 {
    public static void main(String[] args) {
        System.out.println("5!="+recursionAlgorithm(5));
    }

    public static int recursionAlgorithm(int n) {
        if(n==1)
            return 1;
        return n*recursionAlgorithm(n-1);
    }
}
