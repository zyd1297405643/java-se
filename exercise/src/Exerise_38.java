import java.util.Scanner;

/*写一个函数，求一个字符串的长度，在main函数中输入字符串，并输出其长度。
/*………………
*……题目意思似乎不能用length()函数
*/
public class Exerise_38 {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        System.out.println("请输入一个字符串：");
        String str = sc.nextLine();
        System.out.println("字符串的长度是："+str.length());

    }
}
