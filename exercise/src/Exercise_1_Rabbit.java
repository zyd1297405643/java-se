

import java.util.Arrays;

/*题目：古典问题：有一对兔子，从出生后第3个月起每个月都生一对兔子，
   小兔子长到第三个月后每个月又生一对兔子，
   假如兔子都不死，问每个月的兔子总数为多少？*/
public class Exercise_1_Rabbit {
    public static void main(String[] args) {
        int array[]=new int[12];
        array[0]=array[1]=2;
        for(int i=2;i<=11;i++)
           array[i]=array[i-1]+array[i-2];
        System.out.println(Arrays.toString(array));
    }


}
