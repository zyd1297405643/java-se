package exerise_28;

import java.util.Arrays;

/*对10个数进行排序 */
public class Exerise_28_BubbleSort {
    public static void main(String[] args) {
        int array[] = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
            for (int j = 0; j < i; j++) {
                if (array[i] == array[j]) {
                    i--;
                    break;
                }
            }
        }
        System.out.print("原数组");
        System.out.println(Arrays.toString(array));
        System.out.print("排序好的数组");
        //冒泡排序 比较相邻的元素，将小的放到前面
        //•	i 代表次数
        //•	j 代表比较位置
        for (int i = 0; i < array.length-1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp =array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;
                }
            }
        }
        for(int i=0;i<array.length;i++)
            System.out.print(array[i] + "\t");
    }
}
