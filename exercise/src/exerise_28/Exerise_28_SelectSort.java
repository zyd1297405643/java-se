package exerise_28;

import java.util.Arrays;

public class Exerise_28_SelectSort {
    public static void main(String[] args) {
        int array[] = new int[10];
        System.out.print("原数组");
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
            for (int j = 0; j < i; j++) {
                if (array[i] == array[j]) {
                    i--;
                    break;
                }
            }
        }
        System.out.print("原数组");
        System.out.println(Arrays.toString(array));
        System.out.print("排序好的数组");
        //选择排序   每个元素与第一个元素比较
        //•	i 代表第一个数据的位置
        //•	j 代码后部每一个数据的位置
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int temp;
                if (array[i] > array[j]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }

        }
        System.out.println(Arrays.toString(array));
    }
}
