/*题目：输入两个正整数m和n，求其最大公约数和最小公倍数。
在循环中，只要除数不等于0，用较大数除以较小的数，将小的一个数作为下一轮循环的大数，
取得的余数作为下一轮循环的较小的数，如此循环直到较小的数的值为0，返回较大的数，
此数即为最大公约数，最小公倍数为两数之积除以最大公约数。
 */
//Java中与主函数同在一个类中的方法必须是static修饰的方法
import java.util.Scanner;

//12 8
public class Exerise_6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("输人整数m");
        int m = sc.nextInt();
        System.out.println("输入整数n");
        int n = sc.nextInt();
        int greatestCommonDivisor = function(m, n);
        System.out.println("最大公约数" + greatestCommonDivisor);
        int leastCommonMultiple;
        leastCommonMultiple=n*m/greatestCommonDivisor;
        System.out.println("最小公倍数"+leastCommonMultiple);
    }

    public static int function(int num_1, int num_2) {
        if (num_1 < num_2) {
            int temp;
            temp = num_1;
            num_1 = num_2;
            num_2 = temp;
        }
        if (num_1 == num_2) {
            return num_1;
        }
        while (num_2 != 0) {
            int remainder;
            remainder = num_1 % num_2;
            num_1 = num_2;
            num_2 = remainder;

        }
        return num_1;

    }

}




