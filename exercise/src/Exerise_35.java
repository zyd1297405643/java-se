import java.util.Arrays;
import java.util.Scanner;

/*输入数组，最大的与第一个元素交换，最小的与最后一个元素交换，输出数组。 */
public class Exerise_35 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length;
        int index1 = 0;
        int index2 = 0;
        System.out.println("输入数组长度");
        length = sc.nextInt();
        System.out.println("输入各个元素");
        int array[] = new int[length];
        for (int i = 0; i < length; i++) {
            array[i] = sc.nextInt();
        }
        System.out.println("生成的数组" + Arrays.toString(array));
        int max = array[0];
        int min = array[0];

        for (int i = 0; i < length; i++) {
            if (array[i] > max) {
                max = array[i];
                index1 = i;
            }
            if (array[i] < min) {
                min = array[i];
                index2 = i;
            }
        }
        if (index1 != 0) {
            int temp = array[0];
            array[0] = array[index1];
            array[index1] = temp;
        }
        if (index2 != length - 1) {
            int temp = array[length - 1];
            array[length - 1] = array[index2];
            array[index2] = temp;
        }
        for (int i = 0; i < length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
