import java.util.Scanner;

/*一个5位数，判断它是不是回文数。即12321是回文数，个位与万位相同，十位与千位相同。   */
public class Exerise_25 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str;
        System.out.println("输入一个5位数");
        str = sc.nextLine();
        char chars[];
        chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == chars[chars.length - i - 1]) {
                if(i==chars.length/2)
                {
                    System.out.println(str+"是回文");
                }
            }
            else {
                System.out.println(str + "不是回文字符串");
                break;
            }
        }

    }
}
