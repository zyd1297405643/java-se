package e_anonymouslnnerclass.inner;
//前提要求：只想使用一次，并且只想在Outer类中的m1方法中使用
public class MyInterfaceAImpl implements MyInterfaceA{
    @Override
    public void methodAbs1() {
        System.out.println("方法1");
    }

    @Override
    public void methodAbs2() {
        System.out.println("方法2");
    }
}
