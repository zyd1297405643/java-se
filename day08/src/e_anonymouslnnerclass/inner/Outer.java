package e_anonymouslnnerclass.inner;

/*局部内部类 结合 接口使用*/
public   class Outer {

    public void m1() {
        //使用接口中的方法，创建接口的实现类
       /* MyInterfaceAImpl myInterfaceA = new MyInterfaceAImpl();
        myInterfaceA.methodAbs1();
        myInterfaceA.methodAbs2();*/

        class InnerImpl implements MyInterfaceA{
            //在内部类中实现接口中的抽象方法
            @Override
            public void methodAbs1() {
                System.out.println("内部类实现1");
            }

            @Override
            public void methodAbs2() {
                System.out.println("内部类实现2");
            }

        }
        InnerImpl inner = new InnerImpl();
        inner.methodAbs1();
        inner.methodAbs2();
    }


}
