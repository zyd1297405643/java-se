package e_anonymouslnnerclass.inner2;
/*优化
* 实现接口或者继承抽象类*/
public class Outer {
    public static void main(String[] args) {
        /*因为在其他类中用不到innerImpl2，并且只能使用一次
        所以把类名去掉——>没有名字的类——>匿名内部类
         new MyInnerfaceA(){....}-->想要使用的是接口
         又因为接口不能new 对象
         new:创建MyInterfaceA的实现类对象
         现在实现类对象没有名字了，但是还是要实现这个接口
         MyInterfaceA:跟的是想要实现的接口（只是这个实现没有名字）
         {...};这才是真正实现的那个类，没有名字的类
        */

       MyInnerfaceA impl=/* class InnerImpl2 implements*/ new MyInnerfaceA(){
            @Override
            public void methodAbs1() {
                System.out.println("内部实现1");
            }

            @Override
            public void methodAbs2() {
                System.out.println("内部实现2");
            }
        };
       /*现在这个类没有名字，所以返回值类型只能是接口类型*/
        impl.methodAbs1();
        impl.methodAbs2();

        //正常new对象
        Outer outer=new Outer();
        outer.getClass();
        outer.hashCode();

        //匿名对象
         new Outer().getClass();
         new Outer().hashCode();
    }
}
