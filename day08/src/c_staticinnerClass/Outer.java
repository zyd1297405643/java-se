package c_staticinnerClass;

public class Outer {
    //外部类成员变量
    int a = 10;

    //外部类的静态成员变量
    static int b = 20;

    //普通成员方法
    public void m1() {
        new Inner();
        new InnerFiled();
    }

    //静态方法 - 不能直接调用成员内部类
    public static void m2() {
     //new InnerFiled();
        new Inner();
    }

    //成员内部类-非静态内部类
    public class InnerFiled {
    }

    //静态内部类 ，只要加static，都是属于类的
    public static class Inner {
        public void m3() {
            //在静态内部类中不能使用非静态成员
          //  System.out.println(a);
            System.out.println(b);
        }
    }
}
