package c_staticinnerClass;
import c_staticinnerClass.Outer.Inner;
public class Main {
    public static void main(String[] args) {
        //成员内部类创建对象,需要外部类对象
        Outer.InnerFiled innerFiled = new Outer().new InnerFiled();
   //静态内部类创建对象
        Outer.Inner inner = new Outer.Inner();
        //导入一个包import c_staticinnerClass.Outer.Inner;
        Inner inner1=new Outer.Inner();
    }
}
