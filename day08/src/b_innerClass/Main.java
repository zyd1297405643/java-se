package b_innerClass;
/*与成员内部类无关的类*/
public class Main {
    public static void main(String[] args) {
        //在无关类中使用Body的内部类Heart
        Outer body = new Outer();
        //间接调用内部类
        body.swim();

        /*直接调用内部类
        * 通过外部类对象创建内部类对象*/
        Outer.Inner inner=body.new Inner();
       // 外部类.内部类 a=new 外部类().new 内部类();
        Outer.Inner inner1=new Outer().new Inner();
        inner.beat();
    }
}
