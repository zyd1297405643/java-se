package b_innerClass;

/*内部类  之 成员变量*/
public class Outer {
    private int a = 10;
    //普通成员方法

    public void swim() {
        System.out.println("游泳");

        /*外部类使用内部类的东西（成员变量和成员方法）
         直接创建内部类对象*/
        Inner h = new Inner();
        h.beat();
        System.out.println(h.a);
        System.out.println(h.num);

        System.out.println(a);//使用的是外部类的a
    }

    /*成员内部类：拥有的东西和外部类一样*/
    /*private  protected*/public class Inner {
        //属于自己的成员变量
        int num;
        int a = 20;//和外部类的成员变量一样

        //属于内部类自己的成员方法
        public void beat() {
            int a = 30;//局部变量
            /*内部类中可以直接使用外部类的成员
            只有一个不重名变量的时候                             */
            System.out.println("心脏跳动次" + a++);

            //使用内部类的成员a   -this.变量名
            System.out.println("内部类成员变量"+this.a);
            //使用外部类成员变量  外部类.this.变量名
            System.out.println("外部类成员变量" + Outer.this.a);
        }
    }
}
