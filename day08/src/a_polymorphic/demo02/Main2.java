package a_polymorphic.demo02;

/*多态的使用情况*/
public class Main2 {
    public static void main(String[] args) {
        //传递不同的对象，或得到的就是不同的方法
        Animal a = new Dog();
        giveMePet(a);

        giveMePet(new Cat());

          salePet();
    }

    /*  public static void giveMePet(Dog d) {
          d.eat();
          d.watchHouse();
      }

      public static void giveMePet(Cat c) {
          c.catchMouse();
          c.eat();
      }*/

    //Animal 作为参数，可以传递多种类型的对象
    public static void giveMePet(Animal a) {
        if (a instanceof Dog) {

            ((Dog) a).watchHouse();
        } else if (a instanceof Cat) {

            Cat cat = (Cat) a;
            cat.catchMouse();
        }
        a.eat();
    }

    /*Animal 作为返回值类型使用
    * 这个方法就可以返回多种不同类型的对象*/
    public static Animal salePet() {

        return new Cat();
    }
}
