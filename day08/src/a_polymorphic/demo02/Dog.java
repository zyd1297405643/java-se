package a_polymorphic.demo02;

public class Dog extends Animal {
    @Override
    public void eat() {
        System.out.println("狗吃骨头");
    }

    //dog 自己的方法
    public void watchHouse() {
        System.out.println("狗看家");
    }
}
