package a_polymorphic.demo02;

public class Main {
    public static void main(String[] args) {
        Animal d = new Dog();
        d.eat();
        Animal c = new Cat();
        c.eat();

        // 想要调用cat 中的catchMouse方法
        Cat cat = (Cat) c;
        cat.catchMouse();


        //编译看c 是Animal 类型和dog是父子类关系 ，所有语法通过
        //运行时 ，发现c实际上是Cat
        //Dog dog=(Dog) c; //ClassCastException ：类型转换异常

        //关键词：instanceof  问：d引用是不是Dog类型的对象
        //instanceof 左边：引用  右边 ：类型
        if (d instanceof Dog) {
            Dog dog=(Dog) d;
            dog.watchHouse();
        }
        //判断c是不是Dog 的类型
        if (c instanceof Dog) {
            Dog dog=(Dog) c;
            dog.watchHouse();
        }
    }
}
