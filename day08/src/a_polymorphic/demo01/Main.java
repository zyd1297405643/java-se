package a_polymorphic.demo01;
/*测试类
* 多态的形成
*  前提 一定是一个事物的多种形态*/
public class Main {
    public static void main(String[] args) {
        //父类型引用  指向 子类型的对象
        Fu f=new Zi();//向上造型（自动类型转换）
        //Fu f =new Self();//编译错误

        //以下两种都不叫多态
        Zi zi=new Zi();
        Fu fu =new Fu();
    }
}
