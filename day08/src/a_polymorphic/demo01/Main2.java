package a_polymorphic.demo01;
/*多态变现方法 之成员变量
*    成员变量的调用 ，父子类中拥有相同的成员变量
* 并且都有一对get/set 方法
* 如果通过引用直接调用成员变量 -f.age,看左边类型
* 如果通过方法调用成员变量  f.getAge()，看右边对象*/
public class Main2 {
    public static void main(String[] args) {
        Fu f =new Zi();
        //此时获取的是 父类中的age num  ->看左边类型
        System.out.println(f.age);//10
        System.out.println(f.num);//100

        /*此时看的是右边对象有没有这个方法
        * 有就直接显示类中的成员变量的值
        * 没有就向上取
        * 子类中如果重写了getAge() 得到的是子类的值20
        * 子类中如果没有重写getAge() 得到的是10*/
        System.out.println(f.getAge());//20

        //因为父类中没有申明score 变量，不能直接调用
       // System.out.println(f.score);编译失败

       //向下转型（强制类型转换）
        Zi zi= (Zi) f ;
        System.out.println(zi.score);

    }
}
