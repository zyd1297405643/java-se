package a_polymorphic.demo01;

public class Zi extends Fu {
    //和父类同名的成员变量
    int age = 20;
    // 子类自己的单独声明的成员变量
    int score = 150;
    //额外继承的 -num

    //重写父类中的m1方法
    public void m1() {
        System.out.println("Zi m1");
    }
    //继承了父类的m2的方法

    //子类自己打的方法
    public void m3() {

        System.out.println("Zi m3");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
