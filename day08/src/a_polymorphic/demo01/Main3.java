package a_polymorphic.demo01;
/*多态  ———— 成员方法
* 最终调用的是哪个方法 ：
*    优先调用子类重写过的
* 子类如果没有重写 ，就调用父类的方法*/
public class Main3 {
    public static void main(String[] args) {
        Fu f= new Zi();
        f.m1();
        f.m2();
        //m3 没有在Fu中声明 所以不能直接调用
        //f.m3()
        //如果想使用子类自己的方法 ，需要强制转换
        Zi zi =(Zi) f;
        zi.m3();
      }
}
