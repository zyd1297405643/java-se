package a_polymorphic.demo01;

public class Fu {
    int age = 10;
    int num = 100;

    public void m1() {
        System.out.println("Fu m1");
    }

    public void m2() {
        System.out.println( "Fu m2");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
