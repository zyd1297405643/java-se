package d_localinnerclass;

/*内部类 之 局部内部类
 *  即在方法中的类*/
public class Outer {
    //私有变量
    private int b = 20;

    //成员方法
    public void m1() {
        //局部内部类使用外部类方法的变量，默认添加final关键词
        /*final*/
        int d = 10;
        //局部内部类
        /*public protected private*/
        class Inner {
            //局部内部类中的成员方法
            public void iinnerM1() {
                int c = 0;
                System.out.println("成员变量" + b);
                System.out.println("局部内部类m1    "  + d);
            }
        }
        //创建局部内部类对象
        Inner inner = new Inner();
        inner.iinnerM1();
        b++;


    }

}
