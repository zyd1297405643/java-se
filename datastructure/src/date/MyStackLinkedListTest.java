package date;

public class MyStackLinkedListTest {
    public static void main(String[] args) {
        MyStackLinkedList<String> myStack = new MyStackLinkedList();
        System.out.println(myStack.peek());
        System.out.println(myStack.isEmpty());
        myStack.push("1");
        myStack.push("2");
        myStack.push("3");
        myStack.push("4");
        System.out.println(myStack.pop());
        System.out.println(myStack.isEmpty());
        System.out.println(myStack.peek());
    }
}
