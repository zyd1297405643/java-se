package date;

/*1.向集合(this)中末尾添加元素
 2.向集合index的位置中插入obj元素
 3.删除指定位置(index)上的元素，并且返回删除的元素
 4.删除第一个指定元素(obj)
 5.替换指定位置上的元素，替换成obj，并且返回被替换的元素
 6.从集合中获得指定位置(index)的元素
 7.获得集合中的元素个数
 8.判断集合中是否存在指定元素obj
 9.判断集合是否为空：没有有效元素是空
 10.打印出在集合中的有效元素*/
/*链式结构-手动实现LinkList */
public class MyLinkedList {
    //所有的操作只能从头节点出发
    private Node head = new Node();
    private int size = 0;

    //创建节点类  -内部类
    private class Node {
        //节点中数据
        private Object data;
        //下一个节点的引用
        private Node next;
    }

    //1.向集合中的末尾添加元素
    public void add(Object obj) {
        //获得头结点
        Node node = head;
        while (node.next != null) {
            //将node 节点移动到下一个节点
            node = node.next;
        }
        //添加新节点
        Node newNode = new Node();
        //给新节点添加数据
        newNode.data = obj;
        //当前节点的next指向下一个节点
        node.next = newNode;
        //节点数+1
        size++;
    }

    //2.向集合index的位置中插入obj元素
    public void insert(int index, Object obj) {
        if (index < 0 || index > size - 1) {
            return;
        }
        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        Node newNode = new Node();
        newNode.data = obj;
        newNode.next = node.next;
        node.next = newNode;
        size++;
    }

    //3.删除指定位置(index)上的元素，并且返回删除的元素
    public Object remove(int index) {
        if (index < 0 || index > size - 1) {
            System.out.println("删除越界");
            return null;
        }
        Node node = head;
        Object o = null;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        o = node.next.data;
        node.next = node.next.next;
        size--;
        return o;
    }

    //4.删除一个指定元素(obj)
    public void remove(Object obj) {
        Node node = head;
        int index = -1;
        for (int i = 0; i < size; i++) {
            node = node.next;
            if (node.data.equals(obj)) {
                index = i;
            }
            break;
        }
        if (index != -1) {
            remove(index);
        }
        size--;
    }

    //5.替换指定位置上的元素，替换成obj，并且返回被替换的元素
    public Object replace(int index, Object obj) {
        if (index < 0 || index > size - 1) {
            System.out.println("替换越界");
            return null;
        }
        Node node = head;
        Object o = obj;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        node.next.data = obj;
        return o;
    }

    //6.从集合中获得指定位置(index)的元素
    public Object get(int index) {
        if (index < 0 || index > size - 1) {
            System.out.println("查找越界");
            return null;
        }
        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.next.data;
    }

    //7.获得集合中的元素个数
    public Object size() {
        return size;
    }

    //8.判断集合中是否存在指定元素obj
    public boolean contains(Object obj) {
        Node node = head;
        while (node.next != null) {
            node = node.next;
            if (node.data.equals(obj)) {
                return true;
            }
        }

        return false;
    }

    //9.判断集合是否为空：没有有效元素是空
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    // 重写toString() 打印出在集合中的有效元素
    public String toString() {
        Node node = head;
        StringBuilder sb = new StringBuilder("[");
        while (node.next != null) {
            node = node.next;
            sb.append(node.data + ",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        return sb.toString();
    }
}