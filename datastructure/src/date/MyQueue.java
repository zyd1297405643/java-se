package date;

import java.util.Arrays;

/*数组实现对列*/
public class MyQueue {
    private int size = 0;
    Object[] queue;

    public MyQueue() {
        queue = new Object[10];
    }

    public boolean isEmpty() {

        if (size == 0) {
            return true;
        }
        return false;
    }

    public void extendCapacity(int size) {
        if (size > queue.length) {
            size += size >> 1;//扩容原来的一半
            queue = Arrays.copyOf(queue, size);
        }
    }

    public void offer(Object o) {
        extendCapacity(size + 1);
        queue[size] = o;
        size++;
    }

    public Object push() {
        //Object src：the source array. 源数组
        //int srcPos：starting position in the source array. 在源数组中，开始复制的位置
        //Object dest：the destination array. 目标数组
        //int destPos：starting position in the destination data. 在目标数组中，开始赋值的位置
        //int length：the number of array elements to be copied. 被复制的数组元素的数量
        Object o = queue[0];
        System.arraycopy(queue, 1, queue, 0, size - 1);
        size--;
        return "删除队列第一个元素" + o;
    }

    public Object peek() {
        Object o = null;
        if (!isEmpty()) {
            o = queue[0];
        }
        return "队列头元素" + o;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            sb.append(queue[i] + ",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        return sb.toString();

    }
}
