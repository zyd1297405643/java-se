package date;

import java.util.LinkedList;
/*
 * LinkedList实现栈
 *
 */

class MyStackLinkedList<T> {
    private LinkedList<T> ll = new LinkedList<>();
    //入栈
    public void push(T t) {
         ll.addFirst(t);
    }
    //出栈
    public T pop() {

         return ll.removeFirst();
    }

    //栈顶元素
    public T peek() {
        T t=null;
        if(!ll.isEmpty()){
            t=ll.getFirst();
        }
        return t;
    }
    //栈空
    public boolean isEmpty() {
        return ll.isEmpty();
    }
}


