package date;

import java.util.ArrayList;

public class MyArrayListTest {
    public static void main(String[] args) {
        MyArrayList list = new MyArrayList();


        list.add("2");
        list.add("3");
        list.add("4");
        list.insert("1",0);
        System.out.println(list);
        list.remove(4);
        System.out.println("删除规定的下标后"+list);
        list.remove("1");
        System.out.println("删除指定元素后"+list);
        list.replace(2,"5");
        System.out.println(list.get(1));
        System.out.println("替换元素后"+list);
        System.out.println("集合个数"+list.size);
        System.out.println(list.contain("1"));
        System.out.println(list.isEmpty());
        list.printf();
    }
}
