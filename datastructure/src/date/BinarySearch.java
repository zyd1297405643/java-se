package date;

public class BinarySearch {
    public static void main(String[] args) {
        int arr[] = {10, 2, 3, 7, 19, 5, 76, 45, 32, 21};
        int i = serch(arr, 19);
        if (i == -1)
            System.out.println("无");
        else
            System.out.println(i);
    }

    public static int serch(int array[], int key) {
        int high, mid;
        int low = 0;
        mid = 0;
        high = array.length - 1;
        while (low <= high) {
            mid = (high + low) / 2;
            if (key == array[mid]) {
                return mid;
            } else if (key < array[mid]) {
                high = mid - 1;
            } else
                low = mid + 1;

        }
        return -1;
    }
}
