package date;

import java.util.Arrays;

/*
 *顺序结构手动实现 -ArrayList
 * 底层实现：数组
 * 目的:让外面的类看来，它是一个可变长的数组
 * 数组扩容
 * */
public class MyArrayList {
    protected int size = 0;
    private Object[] arr;

    public MyArrayList() {
        arr = new Object[10];
    }
    /*顺序结构 / 链表结构
    实现功能:
    1.向集合(this)中末尾添加元素
    2.向集合index的位置中插入obj元素
    3.删除指定位置(index)上的元素，并且返回删除的元素
    4.删除第一个指定元素(obj)
    5.替换指定位置上的元素，替换成obj，并且返回被替换的元素
    6.从集合中获得指定位置(index)的元素
    7.获得集合中的元素个数
    8.判断集合中是否存在指定元素obj
    9.判断集合是否为空：没有有效元素是空
    10.打印出在集合中的有效元素
  */

    //1.向集合(this)中的末尾添加元素
    public void add(Object obj) {
        if (size == arr.length) {
            arr = Arrays.copyOf(arr, arr.length + 5);
        }
        arr[size] = obj;
        size++;
    }

    //2.向集合index的位置中插入obj元素
    public void insert(Object obj, int index) {
        if (index < 0 || index > size) {
            return;
        }
        //数组扩容
        if (size == arr.length) {
            arr = Arrays.copyOf(arr, arr.length + 5);
        }

        for (int i = size - 1; i >= index; i--) {
            arr[i + 1] = arr[i];
        }
        arr[index] = obj;
        size++;
    }

    //3.删除指定位置(index)上的元素，并且返回删除的元素
    public Object remove(int index) {
        if (index < 0 || index > size - 1) {
            System.out.println("删除的下标越界");
            return null;
        }
        Object o = arr[index];
        for (int i = index; i < size; i++) {
            arr[i] = arr[i + 1];
        }
        size--;
        return o;
    }

    //4.删除一个指定元素(obj)
    public void remove(Object obj) {
        for (int i = 0; i < size; i++) {
            if (arr[i].equals(obj)) {
                remove(i);
            }
        }
    }

    //5.替换指定位置上的元素，替换成obj，并且返回被替换的元素
    public Object replace(int index, Object obj) {
        if (index < 0 || index > size - 1) {
            System.out.println("替换的下标越界");
        }
        Object o = arr[index];
        arr[index] = obj;
        return o;
    }

    //6.从集合中获得指定位置(index)的元素
    public Object get(int index) {
        if (index < 0 || index > size - 1) {
            System.out.println("替换的下标越界");
        }
        return arr[index];

    }

    //7.获得集合中的元素个数
    public Object size() {
        return size;
    }

    //8.判断集合中是否存在指定元素obj
    public boolean contain(Object obj) {
        for (int i = 0; i < size; i++) {
            if (arr[i].equals(obj)) {
                return true;
            }
        }
        return false;
    }

    //    9.判断集合是否为空：没有有效元素是空
    public boolean isEmpty() {
        if (size == 0)
            return true;
        return false;
    }

    //    10.打印出在集合中的有效元素
    public void printf() {
        Object[] array = new Object[size];
        for (int i = 0; i < size; i++) {
            array[i] = arr[i];
        }
        System.out.println(Arrays.toString(array));
    }

    //重写toString 方法
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            sb.append(arr[i] + ",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        return sb.toString();
    }

}
