import java.util.Scanner;

public class ExceptionDemo3 {
    public static void main(String args[]) {
        System.out.println("登录账号,输入用户名、密码");
        Scanner sc = new Scanner(System.in);
        String username = sc.nextLine();
        int password = sc.nextInt();
        try {
            System.out.println("用户名:" + login(username, password));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String login(String username, int password) throws LoginException { //throws Exception
        System.out.println("验证密码");
        if (password == 1) {
            return username;
        } else {
            throw new LoginException("用户名/密码错误");
            /*throw new Exception("用户名/密码错误");*/
        }
    }
}
