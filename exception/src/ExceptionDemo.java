import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExceptionDemo {

    @Test
    public void test() {
        FileInputStream fis = null;
        try {
            new FileOutputStream("a.txt");
            fis = new FileInputStream("a.txt");
            fis.read();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("文件未找到");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.out.println("空指针异常");
        } finally {
            try {
                fis.close();
                System.out.println("关闭流");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
