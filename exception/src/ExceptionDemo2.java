public class ExceptionDemo2 {
    public static void main(String[] args) {
        System.out.println(method());
    }

    public static int method() {
        int i = 1;
        try {
            return ++i;
        } catch (RuntimeException e) {
            return 0;
        } finally {
            i++;

            System.out.println("finally 执行了,i=" + i);
        }

    }
}