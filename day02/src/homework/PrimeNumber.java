package homework;

/*判断101-200之间有多少个素数，并输出所有素数。
  素数：除了1和它本身以外，不能被其他自然数整除的数。   */
public class PrimeNumber {
    public static void main(String[] args) {

        int j;
        int n = 0;
        for (int i = 101; i <= 200; i++) {
            int flag=1;
            for (j = (int) (Math.sqrt(i)); j < i; j++)
            //如果i%j==0,说明i的因子不止1和他本身。
            {
                if (i % j == 0)
                { flag=0;
                    break;
                }
            }
            if (flag == 1) {
                System.out.print(i + "是素数 ");
                n++;
                if (n % 5 == 0)
                    System.out.println();
            }


        }

    }
}
