package homework;
/*古典问题：有一对兔子，从出生后第3个月起每个月都生一对兔子，
小兔子长到第三个月后每个月又生一对兔子，
假如兔子都不死，问每个月的兔子总数为多少？  */
public class Rabbit {
    public static void main(String[] args) {
         int month=1;
         int[] array=new int[100];
         array[0]=1;
         array[1]=1;
        System.out.println("第1个月兔子数"+array[0]*2);
        System.out.println("第2个月兔子数"+array[1]*2);
        for (month=2; month <12; month++) {
            array[month]=array[month-1]+array[month-2];
            System.out.println("第"+(month+1)+"个月兔子数"+array[month]*2);
        }

    }
}
