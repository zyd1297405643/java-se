package ifcase;

import java.util.Scanner;

public class Demo3 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);//获取键盘输入
        int score = sc.nextInt();

            if (score >= 90)
                System.out.println("优");
            else if (score >= 80)
                System.out.println("良");
            else if (score >= 60)
                System.out.println("及格");
            else
                System.out.println("重修");
        }

}
