package ifcase;

import java.util.Scanner;

public class Demo4 {
    public static void main(String[] args) {
        System.out.println("请输入一个年份");
        Scanner sc = new Scanner(System.in);//获取键盘输入
        while (true) {
            int year = sc.nextInt();
            //判断 %4= =0 && %100 !=0 || %400= =0
            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
                System.out.println(year+"年,闰年");
            else
                System.out.println(year+"年,不是闰年");
        }
    }
}
