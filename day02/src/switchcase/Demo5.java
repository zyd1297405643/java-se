package switchcase;
//case 里的数据类型可以是 byte,short,int,char;

import java.util.Scanner;

public class Demo5 {
    public static void main(String[] args) {
        System.out.println("输入月份");
        Scanner sc = new Scanner(System.in);
        while (true) {
            int month = sc.nextInt();
            switch (month) {
                case 2:
                    System.out.println(month + "月,28天");
                    break;
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    System.out.println(month + "月，31天");
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    System.out.println(month + "月，30天");
                    break;
                default:
                    System.out.println(month + "非法月份");
                    break;

            }
        }
    }
}
