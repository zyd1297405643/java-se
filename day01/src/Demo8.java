public class Demo8 {
	public static void main(String[] args) {
		// 位移运算
		int a = -11;
		// 0000 00 101
		// 1*2^3 + 1*2 + 1
		// 1*2^2 + 1
		int b = a >> 1; // 将a向右移动1位
		System.out.println(b); // -6
		int c = a >>> 1; 
		System.out.println(c); // 2147483642

		// 判断的快速使用方式  if
		double d = a > 10 ? 3.14 : 6;

		System.out.println(d);
	}
}