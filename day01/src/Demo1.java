
// 单行注释
/*
  多行注释
 */
/**
  文档注释 用来生成帮助文档
 */
/* Java文件中，一个文件可以有多个class
   但是只能有一个 public 修饰的class
   而且，public 修饰的class 名字必须和文件名一致
   可以没有 public 修饰的class
   如果没有public修饰的class，那么随便一个class和文件名一致即可
 */
public class Demo1{ // 类体
    // 主方法，程序入口
	public static void main(String[] args) {
		// 控制台打印
		System.out.println("重新打印一句话");
	}
}









