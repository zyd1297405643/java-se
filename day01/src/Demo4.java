// 字符类型讲解
public class Demo4 {
	public static void main(String[] args) {
		char c = 48; // ASCII码
		char c1 = 'a' + 1; // 97 
		char c2 = '\u4e2d'; // \u0000 ~ \uffff
		System.out.println(c); //0
		System.out.println(c1);//a
		System.out.println(c2);//中
		// Java第一阶段考试
		// \u7b2c\u4e00\u9636\u6bb5\u8003\u8bd5\u000d\u000a
		System.out.println('\u7b2c');

		// 转义字符   \n-回车  \r-换行  \t-制表符  \\-\
		char cc = '\\';
		System.out.println(cc);
	}
}



