// 浮点类型的讲解
public class Demo3 {
	public static void main(String[] args) {
		// ------- 精度缺失 -------
		// 0.10000000000000009
		double d1 = 3.0 - 2.9;
		System.out.println(d1);
		// 0.09999999999999964
		double d2 = 5.0 - 4.9;
		System.out.println(d2);

		System.out.println(5.0 - 4.5);

		float f = 3.14F;
		float f1 = (float)3.14;
		double d = 3.14d;
	}
}





