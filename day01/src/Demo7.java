public class Demo7 {
	public static void main(String[] args) {
		System.out.println("6 & 9 = " + (6 & 9));
		System.out.println("6 | 9 = " + (6 | 9));
		System.out.println("6 ^ 9 = " + (6 ^ 9));

		// a^b^b = a
		System.out.println("15 ^ 9 = " + (15 ^ 9));

		System.out.println("===========");
		// ---- 逻辑运算符 ----
		int a = 4;
		int b = 9;

		System.out.println(a++ > 4 & --b <= 8);
		System.out.println(a);
		System.out.println(b); // 8 --b运行了
 		// 当前半部分表达式已经能确定整个结果，后半部分就不需要运行，这叫“短路”
		System.out.println(a++ > 4 && --b <= 8);
		System.out.println(a);
		System.out.println(b); // 9 --b没有运行

	}
}