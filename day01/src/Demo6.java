public class Demo6 {
	public static void main(String[] args) {
		// 1. 整除 /
		int a = 3/2; // 1
		System.out.println("a: " + a);
		// 2. + 类型
		byte b = 2;
		// byte c = b + 1; // b+1 -》int类型
		byte b1 = 120;
		// byte c = b + b1; // 编译错误 byte在相加的时候会自动转换成int
		byte c = b ++;
		System.out.println("c: " + c);

		// 3.自加  自减 ++ --
		int d = 10;
		int e = d++; // int e = d; d = d+1
		int f = ++d; // d = d+1; int f = d

		System.out.println("e: " + e); // 10
		System.out.println("f: " + f); // 12

		short s = 127;
		s += 10;
		System.out.println("s: " + s); 



	}
}