
// 1.变量的讲解
// 2.整数类型的讲解
public class Demo2 {
	public static void main(String[] args) {
		// 变量的定义，或者叫声明
		int a;
		a = 1; // 初始化
		// 使用变量  variable a might not have been initialized
		a ++;
		System.out.println(a);
		// a = 3.14; // 改变了变量的类型，编译错误
		System.out.println(a);
		a = 10;

		// -------------
		byte b = 127; // byte范围不能超过-128~127
		short s = 30000;
		int i = 2100000000;
		// 整数直接量默认是int类型
		//long l = 21000000000; - 编译错误
		long l = 21000000000l;
		// 当前系统时间 - 毫秒 1607139388148
		long now = System.currentTimeMillis();
		System.out.println(now);

		// 8进制整数
		int a8 = 012;
		System.out.println(a8);
		// 16进制整数
		int a16 = 0xff;
		System.out.println(a16);

		i = s; // 自动类型转换

		s = (short) i; // 强制类型转换，可能会产生数据缺失
	}
}





