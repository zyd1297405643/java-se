package a_string;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class StringDemo01 {
    public static void main(String[] args) {
        //直接赋值
        String s1 = "hello";
        System.out.println(s1);
        String s2 = new String();
        System.out.println(s2);
        byte[] b = {97, 98, 99};
        String s = new String(b);
        System.out.println(s);
        String s3 = null;
        try {
            s3 = new String(b, "Gbk");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String s4 = new String(s3);
        char[] c = {'h', 'e', 'l', 'l', '0'};
        String s5 = new String(c);
        System.out.println(s5);
        System.out.println("______________________");
        char[] c1 = {'h', 'l'};//hi->字符串->字符数组
        //一定重写过toString
        System.out.println(Arrays.toString(c1));

        /*public String(char[] value,
              int offset,
              int count)
              value - 作为字符源的数组。
              offset - 初始偏移量。
               count - 长度。
                  */
        String s6 = new String(c, 3, 2);
        System.out.println(s6);
        //字符串一旦创建就不可变
        String str = "hello";
        str = "hi";
        String str1 = str;
        System.out.println(str1);

    }
}
