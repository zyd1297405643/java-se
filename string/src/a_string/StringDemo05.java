package a_string;

import java.util.Arrays;
import java.util.regex.Pattern;

public class StringDemo05 {
    public static void main(String[] args) {
        /*Pattern.matches(regex, str)
         */
        //{3,5}至少出现3次，不超过5次
        String str = "[a-zA-z0-9]{3,5}";
        System.out.println(Pattern.matches(str, "12we"));

        String str1 = "[a-z]";
        System.out.println(Pattern.matches(str1,"t"));
        System.out.println(Pattern.matches(str1,"1"));

        //必须以字母开始
        String str2="^[a-z][a-zA-z0-9]{5,17}";
        System.out.println(Pattern.matches(str2,"q12334"));
        System.out.println(Pattern.matches(str2,"112334"));

        //[0-9][0-9]{10},10只与第二个[]有关
        String str3="^1[0-9]{10}";
        System.out.println(Pattern.matches(str3,"18205700493"));

        String email="zyd_1297405643@qq.com";
        //\w 单词字符：[a-zA-Z_0-9]
        String matches="[a-zA-z]\\w{5,17}@qq\\.com";
        System.out.println(Pattern.matches(matches,email));
        String matches1="\\w{5,12}@qq\\.com";
        System.out.println(Pattern.matches(matches1,email));

        /*替换
        * public String replaceAll(String regex, String replacement)
           参数
        regex -- 匹配此字符串的正则表达式。
        newChar -- 用来替换每个匹配项的字符串。*/
        String str4="哈哈哈草,啊操,呵呵艹";
        String str5="(草)|(操)|(艹)";
        String str6=str4.replaceAll(str5,"***");
        System.out.println(str6);

        /*切割 \d 数字：[0-9] */
        String str7="String123Array45object789";
        String regex="\\d+";
        String[] split= str7.split(regex);
        System.out.println(Arrays.toString(split));

    }
}
