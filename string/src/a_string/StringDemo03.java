package a_string;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class StringDemo03 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String str = "你好";
        //字符->字节  编码 看的懂得变成看不懂得
        //byte[] getBytes() -默认方式编码（utf-8）
        byte[] bs = str.getBytes();
        System.out.println(Arrays.toString(bs));//[-28, -67, -96, -27, -91, -67]


        // 字节——>字符 解码 看不懂变成看得懂
        String sUtf = new String(bs, "utf-8");
        String sGbk = new String(bs, "GBK");
        System.out.println(sUtf);//你好
        System.out.println(sGbk);//浣犲ソ

        //解决乱码
        // byte[] getBytes("charset") -自定义字符集编码
        byte[] b = sGbk.getBytes("gbk");
        System.out.println(Arrays.toString(b));//[-28, -67, -96, -27, -91, -67]

        String str1 = new String(b, "utf-8");//你好

        //默认方式解码 utf-8
        String str2 = new String(b);

        System.out.println(str1);
        System.out.println(str2);


    }
}
