package a_string;

public class StringBufferDemo {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer();
        //append()拼接
        sb.append("Runoob..");
        System.out.println(sb);
        sb.append("!");
        System.out.println(sb);
        //insert()插入
        sb.insert(8, "Java");
        System.out.println(sb);
        //delete() 删除
        sb.delete(5,8);
        System.out.println(sb);
        //反转 reverse()
        sb.reverse();
        System.out.println(sb);
    }
}
