package a_string;

public class StringDemo02 {
    public static void main(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("hello");

        //常量池
        String s3 = "hello";
        String s4 = "hello";

        //==比较的是地址值
        System.out.println(s1 == s2);//false
        System.out.println(s1.equals(s2));//true
        System.out.println(s3 == s4);//true
        System.out.println(s3.equals(s4));//true

        String s5 = "hello";
        String s6 = "he";
        String s7 = "he" + "llo";
        String s8 = s6 + "llo";
        System.out.println(s5 == s7);//true
        System.out.println(s5 == s8);//false
    }
}

