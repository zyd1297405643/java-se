package a_string;

public class StringDemo04 {
    public static void main(String[] args) {
        String str = "hello";

        //获取字符串长度
        int l = str.length();
        System.out.println("长度" + l);

        //获得指定下标的字符
        System.out.println(str.charAt(1));

        //将字符串转成字符数组
        char[] chars = str.toCharArray();
        System.out.println(chars);

        //跟字符大小写有关的方法
        String str1 = "gbk";
        String str2 = "Gbk";
        String str3 = "GBk";
        System.out.println(str1.equals(str2));//false

        //全转成大写
        str2 = str2.toUpperCase();
        System.out.println(str2);

        //转成小写
        str2 = str2.toLowerCase();
        System.out.println(str2);

        //忽略大小写 compareToIgnoreCase
            /*如果参数字符串等于此字符串，则返回值 0；
                如果此字符串小于字符串参数，则返回一个小于 0 的值；
                如果此字符串大于字符串参数，则返回一个大于 0 的值。*/
        int result = str1.compareToIgnoreCase(str2);
        System.out.println(result);

        //拼接字符串
        String str4 = "java";
        String str5 = str4.concat("Se");
        System.out.println(str5);

        //判断是否有指定字符 contains(指定字符)
        System.out.println(str4.contains("j"));//true

        //判断长度是否为空isEmpty()
        boolean b=str.isEmpty();
        System.out.println(b);

        //替换指定字符
        String str6=str.replace("h","H");
        System.out.println(str6);

        //截取字符串，substring(int beginindex，int endindex),不包含endindex
        System.out.println(str.substring(2,4));

        //根据给定正则表达式的匹配拆分此字符串。
        String str7 = "one tw,o th,ree fo,ur";
        String[] tokens = str7.split(" |,");
        for (String s: tokens)
            System.out.println(s);

        //startsWith()方法一般用于检测某请求字符串是否以指定的前缀开始的。
        //例如：服务器要判断某个请求是否合规，首先确定协议，
        // 比如http、ftp等，这时，就可以用startsWith()。
        String request = "http://www.baidu.com";
        if (request.startsWith("http")) {
            System.out.println("这是http协议请求！");
        }else if (request.startsWith("ftp") ) {
            System.out.println("这是FTP协议请求！");
        }else {
            System.out.println("请求信息不完整");
        }

        //endsWitch
        if(request.endsWith("com"))
        {
            System.out.println("这是com后缀");
        }
        else
            System.out.println("不是com后缀");


    }
}
