package method_reference.c_structuremethod;

public class Demo {

    public static Person printName(String str, PersonBuilder p) {

        return p.show(str);
    }

    public static void main(String[] args) {
        printName("hello", str -> new Person(str) {
        });
        //第一次优化
        printName("hello", name -> new Person(name));
        //第二次优化 类名::new
        /*1.确定调用的构造方法的对象-在这里是构造方法，没有对象名
         * 2.确定调用的方法是什么-在这理构造方法没有方法名 ，是在new对象时调用的*/
        printName("hello", Person::new);
    }
}
