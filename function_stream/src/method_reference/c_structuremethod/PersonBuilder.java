package method_reference.c_structuremethod;
@FunctionalInterface
public interface PersonBuilder {
   Person show(String str);
}
