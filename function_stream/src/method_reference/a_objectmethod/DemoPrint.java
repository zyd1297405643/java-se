package method_reference.a_objectmethod;

public class DemoPrint {
    public static void printString(String str, Printable p) {
        p.print(str);

    }

    public static void main(String[] args) {
        String str = "hello";
        printString(str, s -> System.out.println(s));

        //优化  JDK自带System.out 对象  println()方法
        //简化   对象名::方法名
        printString(str, System.out::println);
    }

}
