package method_reference.a_objectmethod;
@FunctionalInterface
public interface Printable {
    void print(String str);
}
