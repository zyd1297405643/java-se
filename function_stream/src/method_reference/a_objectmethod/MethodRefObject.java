package method_reference.a_objectmethod;

public class MethodRefObject {
    public void printUpperCase(String str) {
        System.out.println(str.toUpperCase());
    }

    public static void show(String str, Printable p) {
        p.print(str);
    }

    public static void main(String[] args) {
        MethodRefObject methodRefObject = new MethodRefObject();
        // methodRefObject  调用成员方法 printUpperCase()
        show("hi", str -> {
            methodRefObject.printUpperCase(str);
        });
        //一次优化
        show("hi", str -> methodRefObject.printUpperCase(str));
        //二次优化 方法引用  对象::成员方法
        show("hi", methodRefObject::printUpperCase);
    }
}
