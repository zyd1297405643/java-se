package method_reference.thismetod;

public class Demo {
    public static void method(ThisInterface t) {
        t.buyAll();

    }

    public void buy() {
        System.out.println("买房子");
    }

    public void show() {
        //lambda 表达式
        method(() -> this.buy());
        //消除 lambda 表达式
        method(this::buy);
    }

    public static void main(String[] args) {
        Demo d = new Demo();
        d.show();
    }
}
