package method_reference.f_arraymethod;
@FunctionalInterface
public interface ArrayInterface {
   int[] builder(int n);
}
