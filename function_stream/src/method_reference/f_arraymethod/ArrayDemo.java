package method_reference.f_arraymethod;

import java.util.Arrays;

public class ArrayDemo {
    public static int[] method(int n, ArrayInterface arrayInterface) {
        return arrayInterface.builder(n);
    }

    public static void main(String[] args) {
        method(10, n -> new int[n]);
        //简化
        int[] method = method(10, int[]::new);

        System.out.println(Arrays.toString(method));
    }


}
