package method_reference.b_staticmethod;

@FunctionalInterface
public interface MathInterface {
    int Math(int num);
}
