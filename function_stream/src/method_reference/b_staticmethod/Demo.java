package method_reference.b_staticmethod;

public class Demo {
    /*java.lang.Math 类中已经存在了静态⽅法 abs */
    public static void method(int num, MathInterface m) {
        System.out.println(m.Math(num));

    }

    public static void main(String[] args) {
        method(-16, num -> (int) Math.abs(num));
        //简化
    /*    Lambda表达式： n -> Math.abs(n)
         ⽅法引⽤： Math::abs*/
        method(-16, Math::abs);
    }
}
