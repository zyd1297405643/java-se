package method_reference.e_supermethod;

public class Zi extends Fu{
    public void method(String messege) {
        System.out.println("hello:" + messege);
    }
    public static void get(String str,M m){
             m.print(str);
    }
    public void say(){
        get("luck",s->super.method(s));
        //优化
        get("luck",super::method);
        //子类
        get("luck",this::method);
    }

    public static void main(String[] args) {
        new Zi().say();
    }
}
