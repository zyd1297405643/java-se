package method_reference.e_supermethod;
@FunctionalInterface
public interface M {
    void print(String string);
}
