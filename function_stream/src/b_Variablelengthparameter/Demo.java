package b_Variablelengthparameter;

public class Demo {
    public static void main(String[] args) {
        getSum(1, 2);
        System.out.println(getSum(1.0, new int[]{1, 2, 3, 5}));
        System.out.println(getSum(0, 1, 2, 3, 4));
    }

    public static int getSum(int a, int b) {
        return a + b;
    }

    public static double getSum(double b, int... a) {
        double sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }

    public static double getSum(int b, int... a) {
        double sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }
}
