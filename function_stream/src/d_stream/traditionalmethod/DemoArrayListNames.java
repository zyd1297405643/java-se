package d_stream.traditionalmethod;

import java.util.ArrayList;
import java.util.List;

public class DemoArrayListNames {
    /*1. 第⼀个队伍只要名字为3个字的成员姓名；存储到⼀个新集合中。
     2. 第⼀个队伍筛选之后只要前3个⼈；存储到⼀个新集合中。
     3. 第⼆个队伍只要姓张的成员姓名；存储到⼀个新集合中。
    4. 第⼆个队伍筛选之后不要前2个⼈；存储到⼀个新集合中。
    5. 将两个队伍合并为⼀个队伍；存储到⼀个新集合中。
    6. 根据姓名创建 Person 对象；存储到⼀个新集合中。
    7. 打印整个队伍的Person对象信息*/
    public static void main(String[] args) {
        //第⼀⽀队伍
        ArrayList<String> one = new ArrayList<>();
        one.add("迪丽热巴");
        one.add("宋远桥");
        one.add("苏星河");
        one.add("⽯破天");
        one.add("⽯中⽟");
        one.add("⽼⼦");
        one.add("庄⼦");
        one.add("洪七公");
        ArrayList<String> list = new ArrayList<>();
        for (String name : one) {
            if (name.length() == 3) {
                list.add(name);
            }
        }
        System.out.println("三个字的人:" + list);
        List<String> newList = list.subList(0, 3);
        System.out.println("筛选后前3人:" + newList);

        //第⼆⽀队伍
        ArrayList<String> two = new ArrayList<>();
        two.add("古⼒娜扎");
        two.add("张⽆忌");
        two.add("赵丽颖");
        two.add("张三丰");
        two.add("尼古拉斯赵四");
        two.add("张天爱");
        two.add("张⼆狗");
        // ....
        ArrayList<String> list1 = new ArrayList<>();
        for (String name : two) {
            if (name.startsWith("张")) {
                list1.add(name);
            }
        }
        System.out.println("姓张的人:" + list1);
        List<String> newList1 = list1.subList(2, list1.size());
        System.out.println("筛选后不要前2个人" + newList1);

        List<String> newList2 = new ArrayList<>();
        newList2.addAll(newList);
        newList2.addAll(newList1);
        System.out.println("合并后成员:" + newList2);

        List<Person> personList = new ArrayList<>();
        for (String name : newList2) {
            personList.add(new Person(name));
        }

        System.out.println(personList);
    }

}
