package d_stream.newmethod;

import java.util.stream.Stream;

public class LimitDemo {
    public static void main(String[] args) {
        Stream<String> original = Stream.of("张⽆忌", "张三丰", "周芷若");
        Stream<String> result = original.limit(2);
        System.out.println(result.count()); // 2
    }
}
