package d_stream.newmethod;

import java.util.stream.Stream;

/*拼接*/
public class ConcatDemo {
    public static void main(String[] args) {
        Stream<String> streamA = Stream.of("张⽆忌");
        Stream<String> streamB = Stream.of("张翠⼭");
        Stream<String> result = Stream.concat(streamA, streamB);
        System.out.println(result.count());
    }
}
