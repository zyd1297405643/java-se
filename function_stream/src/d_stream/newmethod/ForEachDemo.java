package d_stream.newmethod;

import java.util.stream.Stream;

public class ForEachDemo {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("张⽆忌", "张三丰", "周芷若");
        stream.forEach(name -> System.out.println(name));
        stream.forEach(System.out::println);
    }
}
