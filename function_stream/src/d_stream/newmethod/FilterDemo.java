package d_stream.newmethod;

import java.util.stream.Stream;

public class FilterDemo {
    public static void main(String[] args) {
        Stream<String> original = Stream.of("张⽆忌", "张三丰", "周芷若", "张三");
        Stream<String> result = original.filter(s -> s.startsWith("张"));
        result.forEach(s -> System.out.println(s));
    }
}
