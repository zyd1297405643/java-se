package d_stream.newmethod;

import d_stream.traditionalmethod.Person;

import java.util.ArrayList;
import java.util.stream.Stream;

/*综合*/
public class Comprehensive {
    public static void main(String[] args) {
        /*1. 第⼀个队伍只要名字为3个字的成员姓名；存储到⼀个新集合中。
     2. 第⼀个队伍筛选之后只要前3个⼈；存储到⼀个新集合中。
     3. 第⼆个队伍只要姓张的成员姓名；存储到⼀个新集合中。
    4. 第⼆个队伍筛选之后不要前2个⼈；存储到⼀个新集合中。
    5. 将两个队伍合并为⼀个队伍；存储到⼀个新集合中。
    6. 根据姓名创建 Person 对象；存储到⼀个新集合中。
    7. 打印整个队伍的Person对象信息*/
        //第⼀⽀队伍
        ArrayList<String> one = new ArrayList<>();
        one.add("迪丽热巴");
        one.add("宋远桥");
        one.add("苏星河");
        one.add("⽯破天");
        one.add("⽯中⽟");
        one.add("⽼⼦");
        one.add("庄⼦");
        one.add("洪七公");
        ArrayList<String> two = new ArrayList<>();
        two.add("古⼒娜扎");
        two.add("张⽆忌");
        two.add("赵丽颖");
        two.add("张三丰");
        two.add("尼古拉斯赵四");
        two.add("张天爱");
        two.add("张⼆狗");

        Stream<String> stream = one.stream().filter(name -> name.length() == 3).limit(3);

        //第⼆⽀队伍
        Stream<String> stream1 = two.stream().filter(name -> name.startsWith("张")).skip(2);

        Stream.concat(stream, stream1).map(name -> new Person(name)/*Person::new*/).forEach(System.out::println);


    }
}
