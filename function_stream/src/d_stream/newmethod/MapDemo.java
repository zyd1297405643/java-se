package d_stream.newmethod;

import java.util.stream.Stream;

//    ⼀种T类型转换成为R类型，⽽这种转换的动作，就称为“映射”。
//<R> Stream<R> map(Function<? super T, ? extends R> mapper);
public class MapDemo {
    public static void main(String[] args) {
        Stream<String> original = Stream.of("10", "12", "18");
        Stream<Integer> result = original.map(str -> Integer.valueOf(str));
    }
}
