package d_stream.newmethod;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
        List<String> list = List.of("A", "B", "C", "D");
        Stream<String> stream1 = list.stream();
        //遍历
        stream1.forEach(s -> System.out.println(s));
        System.out.println("-------------------------");
        Set<String> set = Set.of("A", "B", "C", "D");
        Stream<String> stream2 = set.stream();
        stream2.forEach(s -> System.out.println(s));
        Map<Integer,String > map= Map.of(1,"你好",2,"再见");
        //获得Map的Key值
        Stream<Integer> stream3 = map.keySet().stream();
        stream3.forEach(s-> System.out.println(s));
        //获得Map的Value值
        Stream<String> stream4 = map.values().stream();
        stream4.forEach(s-> System.out.println(s));
        //获得map的key和value
        Stream<Map.Entry<Integer, String>> stream5 = map.entrySet().stream();
        stream5.forEach(s-> System.out.println(s));

        int arr[]={1,2,3,4,5};
        Stream.of(arr).forEach(s-> System.out.println(Arrays.toString(s)));

    }

}
