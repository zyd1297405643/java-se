package c_functionalinterface.a_supplier;

import java.util.function.Supplier;

public class SuppierDemo1 {
    public static int getMax(Supplier<Integer> max) {
        return max.get();
    }

    public static void main(String[] args) {
        int max1 = getMax(()-> {
            int array[] = new int[]{1, 2, 4, 9, 0};
            int max = 0;
            for (int i = 0; i < array.length; i++) {
                if (max < array[i]) {
                    max = array[i];
                }

            }
            return max;
        });
        System.out.println(max1);
    }
}
