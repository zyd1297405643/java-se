package c_functionalinterface.a_supplier;

import java.util.function.Supplier;

public class SupplierDemo {
    /*Supplierv是一个函数式接口
     * 可以用lambda来简化，前提是将Supolier对象作为方法参数*/

    public static String getString(Supplier<String> su) {
        return su.get();
    }

    public static void main(String[] args) {
        String s = "你";
        String s1 = "好";
        System.out.println(getString(new Supplier<String>() {
            @Override
            public String get() {
                return s + s1;
            }
        }));
        //lambda 简化
        String string = getString(() -> s + s1);
        System.out.println(string);
    }
}
