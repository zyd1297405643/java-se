package c_functionalinterface.d_funcation;

import java.util.function.Function;

public class FunctionDemoandThen {
 /*   请使⽤ Function 进⾏函数模型的拼接，按照顺序需要执⾏的多个函数操作为：
    String str = "赵丽颖, 20";
             1. 将字符串截取数字年龄部分，得到字符串；  Funcation<String,String>
            2. 将上⼀步的字符串转换成为int类型的数字；   Funcation<String,Integer>
            3. 将上⼀步的int数字累加100，得到结果int数字 Funcation<Integer,Integer>*/

    public static int tranform(String name, Function<String, String> fun1,
                               Function<String, Integer> fun2,
                               Function<Integer, Integer> fun3) {
        return fun1.andThen(fun2).andThen(fun3).apply(name);

    }

    public static void main(String[] args) {
        String name = "小乔,20";
        int result = tranform(name, s -> s.split(",")[1], s -> Integer.valueOf(s), i -> i + 100);
        System.out.println(result);
    }
}
