package c_functionalinterface.c_predicate;

import java.util.function.Predicate;

/*negate 非*/
public class PredicateNegate {
    public static boolean judge(String name, Predicate<String> p) {

        return p.negate().test(name);
    }

    public static void main(String[] args) {
        String name = "qwqeq";
        boolean judge = judge(name, p -> p.length() > 5);
        System.out.println(judge);

    }
}



