package c_functionalinterface.c_predicate;

import java.util.function.Predicate;

public class PredicateDemo {
    public static boolean judge(String name, Predicate<String> p) {

        return p.test(name);
    }

    public static void main(String[] args) {
        String name = "qwqeq";
        boolean judge = judge(name, new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() >= 5;
            }
        });
        //简化
        boolean judge1 = judge(name, p -> p.length() > 5);
        System.out.println(judge);
        System.out.println(judge1);
    }
}
