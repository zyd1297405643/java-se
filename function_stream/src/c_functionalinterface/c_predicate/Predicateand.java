package c_functionalinterface.c_predicate;

import java.util.function.Predicate;

public class Predicateand {
    public static boolean limit(String string, Predicate<String> preName,
                                Predicate<String> preSex) {
        return preName.and(preSex).test(string);
    }

    public static void main(String[] args) {
        String[] array = {"迪丽热巴,女", "张三,女", "玛卡巴卡,女", "唔西迪西,男"};
        for (String string : array) {
            boolean b = limit(string, new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return s.split(",")[0].length() == 4;
                }
            }, new Predicate<String>() {
                @Override
                public boolean test(String sex) {
                    return sex.split(",")[1].equals("女");
                }
            });
            //lambda简化
            limit(string, name -> name.split(",")[0].length() == 4,
                    sex -> sex.split(",")[1].equals("女"));
            if (b)
                System.out.println(string);
        }


    }


}

