package c_functionalinterface.b_consumer;

import java.util.function.Consumer;

public class ConsumerandThen {
    public static void calculation1(int n, Consumer<Integer> i, Consumer<Integer> i1) {
        i.andThen(i1).accept(n);
    }

    public static void main(String[] args) {
        int n = 10;
        calculation1(n, (integer -> System.out.println(++integer)),
                (integer -> System.out.println(integer + 10)));
    }
}
