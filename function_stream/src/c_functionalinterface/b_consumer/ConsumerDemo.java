package c_functionalinterface.b_consumer;

import java.util.function.Consumer;

public class ConsumerDemo {
    /*Consumer是一个函数式接口
     * 可以用lambda来简化，前提是将Consumer对象作为方法参数*/
    public static void calculation(int n, Consumer<Integer> i) {
        i.accept(n);
    }

    public static void main(String[] args) {
        int n = 10;
        calculation(n, new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(++integer);
            }
        });
        //lambda 简化
        calculation(n, (integer) -> System.out.println(++integer));


    }
}
