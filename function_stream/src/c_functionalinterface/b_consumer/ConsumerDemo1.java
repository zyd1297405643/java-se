package c_functionalinterface.b_consumer;

import java.util.function.Consumer;

public class ConsumerDemo1 {

    public static void imfor(String[] array, Consumer<String> con) {
        for (String str : array) {
            con.accept(str);
        }
    }

    public static void main(String[] args) {
        String array[] = {"张三,男", "李四,女"};
        imfor(array, new Consumer<String>() {
            @Override
            public void accept(String s) {
                String[] strings = s.split(",");
                System.out.print("姓名:" + strings[0] + " ");
                System.out.print("性别:" + strings[1] + ";");

            }
        });
        System.out.println();
        //lambda 简化
        imfor(array, s -> {
            String[] strings = s.split(",");
            System.out.print("姓名:" + strings[0] + " ");
            System.out.print("性别:" + strings[1] + ";");
        });
    }
}
