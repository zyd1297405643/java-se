package a_properties;

import java.io.IOException;
import java.util.Properties;

public class PropertiesDemo {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            //读取配置文件 key-value 自动装配到properties对象中
            properties.load(PropertiesDemo.class.getResourceAsStream("config.properties"));
            //properties.load(new FileInputStream("properties\\src\\a_properties\\config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //获得对应的值

        String username = properties.getProperty("username");
        System.out.println(username);

    }

}
