package e_join;

public class JoinDemo {

    public static void main(String[] args) {
        Thread load = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i <= 100; i++) {
                    System.out.println("正在缓冲....." + i + "%");
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("加载完成");

            }
        };

        Thread show = new Thread() {
            @Override
            public void run() {
                System.out.println("等待加载完成");
                try {
                    load.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("显示图片");
            }


        };

        load.start();
        show.start();
    }
}
