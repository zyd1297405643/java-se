package lock.synchronizedlock;

public class Tickets {
    public int tickets = 100;


    public /*synchronized*/ void saleTickets() {

        synchronized (this) {

            if (tickets > 0) {
                try {
                    Thread.sleep((int) (Math.random() * 100));
                } catch (InterruptedException e) {
                    //InterruptedException  中断异常
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() +
                        "售出第" + tickets-- + "张票,还剩票数" + tickets);
            }

        }
    }
}



