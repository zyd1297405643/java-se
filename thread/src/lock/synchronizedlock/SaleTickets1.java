package lock.synchronizedlock;

public class SaleTickets1 implements Runnable {
    int ticket = 10;
    Object lock = new Object();

    @Override
    public void run() {

           synchronized (lock) {
            while(ticket > 0) {
                    try {
                        Thread.sleep((int) (Math.random() * 100));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                System.out.println(Thread.currentThread().getName() +
                        "售出第" + ticket--+"张票,还剩"+ticket+"张");
                }
            }
        }
}


