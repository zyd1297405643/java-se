package lock.synchronizedlock;

public class SaleTickets implements Runnable {
    Tickets T;

    public SaleTickets(Tickets t) {
        T = t;
    }

    @Override
    public void run() {
        while (T.tickets > 0) {
            T.saleTickets();
        }
    }
}
