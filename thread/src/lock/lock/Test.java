package lock.lock;

import lock.synchronizedlock.SaleTickets;
import lock.synchronizedlock.Tickets;

public class Test {
    public static void main(String[] args) {
        lock.synchronizedlock.Tickets tickets = new Tickets();
        SaleTickets saleTickets = new SaleTickets(tickets);
        Thread t1 = new Thread(saleTickets, "窗口1");
        Thread t2 = new Thread(saleTickets, "窗口2");
        Thread t3 = new Thread(saleTickets, "窗口3");
        t1.start();
        t2.start();
        t3.start();
    }
}
