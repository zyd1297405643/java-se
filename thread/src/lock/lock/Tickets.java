package lock.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*Lock锁也称同步锁，加锁与释放锁⽅法化了，如下：
public void lock() ：加同步锁。
public void unlock() ：释放同步锁*/
public class Tickets {
    public int tickets = 100;
    Lock lock = new ReentrantLock();

    public /*synchronized*/ void saleTickets() {


        lock.lock();
        if (tickets > 0) {
            try {
                Thread.sleep((int) (Math.random() * 100));
            } catch (InterruptedException e) {
                //InterruptedException  中断异常
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() +
                    "售出第" + tickets-- + "张票,还剩票数" + tickets);
        }
        lock.unlock();
    }
}



