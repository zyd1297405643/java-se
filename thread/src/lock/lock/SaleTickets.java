package lock.lock;

import lock.synchronizedlock.Tickets;

public class SaleTickets implements Runnable {
    lock.synchronizedlock.Tickets T;

    public SaleTickets(Tickets t) {
        T = t;
    }

    @Override
    public void run() {
        while (T.tickets > 0) {
            T.saleTickets();
        }
    }
}
