package b_runnable;

/*1.自定义类 implements Runnable 接口
  2.实现run方法*/
public class MyThread implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getId() + "-" +
                    Thread.currentThread().getName() + ":" + i);
        }

    }
}
