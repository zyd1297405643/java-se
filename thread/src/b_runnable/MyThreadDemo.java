package b_runnable;


public class MyThreadDemo {
    public static void main(String[] args) {
        MyThread task = new MyThread();
        //Thread(Runnable target, String name)
        // 分配新的 Thread 对象。
        Thread thread = new Thread(task, "T1");
        Thread thread1 = new Thread(task, "T2");
        thread.start();
        thread1.start();
    }
}
