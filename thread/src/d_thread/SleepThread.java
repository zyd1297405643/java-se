package d_thread;
/*public static void sleep(long millis) ：使当前正在执⾏的线程以指定的毫秒数暂停
        （暂时停⽌执⾏）*/
public class SleepThread {
    public static void main(String[] args) {

        //本质就是继承Thread类
        Thread t1 = new Thread("t1") {
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(this.getId() + "-" + this.getName() + ":" + i);
                }
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        //本质就是实现Runnable接口
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getId() + "-" + Thread.currentThread().getName() + ":" + i);
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t2");

        t1.start();
        t2.start();
    }
}
