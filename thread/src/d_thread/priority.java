package d_thread;

public class priority {
    public static void main(String[] args) {
        MyThread task = new MyThread();
        Thread t1 = new Thread(task, "t1");
        Thread t2 = new Thread(task, "t2");
        Thread t3 = new Thread(task, "t3");
        /*优先级：setPriority()
 优先级1~10 数值越大优先级越高 ，默认优先级5
 作用:提高cpu获取该线程的概率*/
        t1.setPriority(10);
        t3.setPriority(1);
        t1.start();
        t2.start();
        t3.start();

    }

}
