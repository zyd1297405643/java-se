package c_innercalssthread;

public class InnerClassThread {
    public static void main(String[] args) {
        //本质就是继承Thread类
        Thread t1 = new Thread("t1") {
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(this.getId() + "-" + this.getName() + ":" + i);
                }
            }
        };
        //本质就是实现Runnable接口
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getId() + "-" + Thread.currentThread().getName() + ":" + i);
                }
            }
        }, "t2");
        t1.start();
        t2.start();
    }
}
