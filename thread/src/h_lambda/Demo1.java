package h_lambda;

import java.util.ArrayList;
import java.util.List;

public class Demo1 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("lucy");
        list.add("jack");
        list.add("rose");

        for (String str : list) {
            System.out.println(str);
        }
        System.out.println("============");
        list.stream().forEach((str) -> System.out.println(str));
        System.out.println("============");
        list.stream().forEach(System.out::println);
        System.out.println("============");
        list.forEach(System.out::println);
    }


}
