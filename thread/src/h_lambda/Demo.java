package h_lambda;

public class Demo {
    public static void main(String[] args) {
        MyInterface myInterface = new MyInterface() {
            @Override
            public int getSum(int a, int b) {
                return a + b;
            }
        };
        method(myInterface);
        //简化写法
        method(new MyInterface() {
            @Override
            public int getSum(int a, int b) {
                return a + b;
            }
        });
        // lambda 可推导, 可省略
        // 1.要实现的接口和接口的方法是确定的, 所以可省略, 只保留了参数列表和方法体
        method((int a, int b) -> {
            return a + b;
        });
        // 2.方法参数类型可以省略
        method((a,b) -> {
            return a + b;
        });
        // 3.前提:方法体中只有一句话, 省略return关键字, 分号, 方法体大括号
        method((a, b) -> a + b);
    }
    // 这个方法没有意义, 就是为了让MyInterface作为方法参数
    public static void method(MyInterface m) {
        m.getSum(1, 2);
    }
}
