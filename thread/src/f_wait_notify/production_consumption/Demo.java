package f_wait_notify.production_consumption;

public class Demo {
    public static void main(String[] args) {
        Product product = new Product();
        Production production = new Production(product);
        Consumer consumer = new Consumer(product);
        production.start();
        consumer.start();
    }
}
