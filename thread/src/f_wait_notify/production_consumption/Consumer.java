package f_wait_notify.production_consumption;

public class Consumer extends Thread {
    /*包⼦铺线程⽣产包⼦，吃货线程消费包⼦。
    1.当包⼦没有时( 包⼦状态为false )，吃货线程等
    待
    2.包⼦铺线程⽣产包⼦( 即包⼦状态为true )，并通知吃货线程( 解除吃货的等待状态 )
    3.有包⼦时，那么包⼦铺线程进⼊等待状态。接下来，吃货线程能否进⼀步执⾏则取决于
    锁的获取情况。
   如果吃货获取到锁，那么就执⾏吃包⼦动作，包⼦吃完( 包⼦状态为false )，
   并通知包⼦铺线程( 解除包⼦铺的等待状态 )，吃货线程进⼊等待。包⼦铺线程能否进⼀步执⾏
    则取决于锁的获取情况。*/
    Product product;

    public Consumer(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public void run() {

        while (true) {
            if (product.isEmpty == true) {
                synchronized (product) {
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
            //有货
            System.out.println("消费");
            product.count--;
            if (product.count == 0) {
                System.out.println("消费者:厂家没货了");
                product.isEmpty= true;
                synchronized (product) {
                    product.notify();
                }
            }
        }
    }
}
