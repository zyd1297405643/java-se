package f_wait_notify;

public class LoadPicture extends Thread {

    Picture picture = new Picture();

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public LoadPicture(Picture picture) {
        this.picture = picture;
    }

    //加载图片
    @Override
    public void run() {
        System.out.println("等待加载");
        for (int i = 0; i <= 100; i++) {
            System.out.println("正在加载....." + i + "%");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("加载完成");
        //加载完成
        picture.isLoad = true;
        //唤醒show线程
        synchronized (picture) {
            picture.notify();
        }
        //等待图片显示
        if (!picture.isShow) {
            synchronized (picture) {
                try {
                    picture.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        //显示完成，下载图片
        for (int i = 0; i <= 100; i++) {
            System.out.println("正在下载....." + i + "%");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("下载完成");


    }
};

