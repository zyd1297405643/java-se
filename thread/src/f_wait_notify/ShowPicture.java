package f_wait_notify;

public class ShowPicture extends Thread {
    Picture picture = new Picture();

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public ShowPicture(Picture picture) {
        this.picture = picture;
    }

    //等待加载完成
    @Override
    public void run() {
        if (!picture.isLoad) {
            synchronized (picture) {
                try {
                    picture.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("显示图片");
            }
        }
        picture.isShow = true;
        //唤醒loadPicture线程
        synchronized (picture) {
            picture.notify();
        }
    }


};
