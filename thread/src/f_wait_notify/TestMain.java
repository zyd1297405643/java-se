package f_wait_notify;

public class TestMain {
    public static void main(String[] args) {
        Picture picture = new Picture();
        //picture 共享资源
        Thread load = new LoadPicture(picture);
        Thread show = new ShowPicture(picture);
        load.start();
        show.start();
    }
}
