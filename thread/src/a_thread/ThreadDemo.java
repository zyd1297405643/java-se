package a_thread;

public class ThreadDemo {
    public static void main(String[] args) {
        //创建线程对象
        MyThread myThread = new MyThread("t1");
        MyThread myThread1 = new MyThread("t2");
        //开启线程
        myThread.start();
        myThread1.start();
        for (int i = 0; i < 10; i++) {
            //currentThread()
            //返回对当前正在执行的线程对象的引用。
            System.out.println(Thread.currentThread().getId() + "-" + Thread.currentThread().getName() + ":" + i);
        }
    }

}
