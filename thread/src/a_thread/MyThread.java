package a_thread;

/*方式一
* 1.自定义线程类 extends Thread
* 2.重写run方法
* 3.run方法中定义线程的任务*/
public class MyThread extends Thread{
    public MyThread(String name) {
        super(name);
    }

    @Override
    //this 表示当前线程对象
    public void run(){
        for (int i = 0; i <10 ; i++) {
            System.out.println(this.getId()+"-"+this.getName()+":"+i);
        }
    }

}
