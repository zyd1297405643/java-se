package g_Executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyRunnable implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + ":" + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        //创建Runaable 实例对象
        MyRunnable myRunnable = new MyRunnable();
        //创建线程池对象
        ExecutorService service = Executors.newFixedThreadPool(4);
//        从线程池中获取线程对象,然后调⽤MyRunnable中的run
        service.submit(myRunnable);
        // 再获取个线程对象，调⽤MyRunnable中的run()
        service.submit(myRunnable);
        service.submit(myRunnable);
        service.submit(myRunnable);
        // 注意:submit⽅法调⽤结束后，程序并不终⽌，是因为线程池控制了线程的关闭。
        // 将使⽤完的线程⼜归还到了线程池中
        //关闭
        service.shutdown();
    }


}
