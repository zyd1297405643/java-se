package g_Executor;

import java.util.Date;
import java.util.concurrent.*;

public class MyCallable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Date> call = new Callable() {
            @Override
            public Date call() throws Exception {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName() + ":" + i);
                }
                Thread.sleep(500);
                return new Date();
            }

        };
        //固定线程数
        ExecutorService service = Executors.newFixedThreadPool(4);
        service.submit(call);
        service.submit(call);
        service.submit(call);
        Future<Date> future = service.submit(call);
        System.out.println(future.get());
        service.shutdown();
    }
}
