package staticcodeblock;

public class Son extends Fu{
    static{
        System.out.println("子类的静态代码块");
    }
    {
        System.out.println("子类代码块");
    }
    public Son() {
        System.out.println("这是子类的无参构造方法");
    }
    public Son(int aaa,int bbb){
        this.aaa=aaa;
        this.bbb=bbb;
        System.out.println("这是子类中的有参构造方法");
    }
}


