package staticcodeblock;

public class Fu {
    int aaa;
    int bbb;
    static{
        System.out.println("父类的静态代码块");
    }
    {
        System.out.println("父类代码块");
    }
    public Fu() {
        System.out.println("这是父类的无参构造方法");
    }
    public Fu(int aaa,int bbb){
        this.aaa=aaa;
        this.bbb=bbb;
        System.out.println("这是父类中的有参构造方法");
    }
}
