package staticcodeblock;
/*静态代码块
 * 1。属于类的
 * 2.类使用的情况共三种 ，但是类只加载一次，只执行一次
 * 3.作用:给静态成员变量初始化
 * 4.静态代码块可以有多个，但是没有必要*/

/*代码块
* 1.属于对象的，创建多少个对象代码块就多少*
2.作用:给成员变量初始化
3.在构造方法使用前执行/
 */
public class Demo04 {
int age;
String name;
    //静态代码块
    static {
        System.out.println("静态代码块执行了");
    }
    //代码块
    {
        System.out.println("代码块执行了");
    }

    public Demo04() {
        System.out.println("无参构造方法执行了");
    }

    public Demo04(int age) {
        System.out.println("有参构造方法执行了" );
    }


}
