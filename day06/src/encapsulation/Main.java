package encapsulation;

public class Main {
    public static void main(String[] args) {
        Demo01 demo01=new Demo01();
        int age=demo01.getAge();
        System.out.println(age);

        demo01.setAge(18);
        System.out.println(demo01.getAge());

        String name= demo01.getName();
        System.out.println(name);

        demo01.setName("qwqe");
        System.out.println(demo01.getName());
    }
}
