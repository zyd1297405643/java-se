package staticvariable;
/*静态成员变量：
 * 1.属于类的
 * 2.类名.属性调用—>Demo02.money 方便在没有创建对象的情况下来进行调用（方法/变量）
 * 3.只有一个，只分配一次内存
 * 4.静态变量可以同时被多个实例所共享*/
public class Main {
    public static void main(String[] args) {
        Demo02 demo02=new Demo02();
        Demo02 demo03=new Demo02();
        System.out.println(Demo02.money);
        Demo02.money-=10;
        System.out.println(Demo02.money);
//        静态变量可以同时被多个实例所共享
        System.out.println(demo02.money);
        System.out.println(demo03.money);

        demo02.age -= 9;
        System.out.println(demo02.age);

        System.out.println(demo03.age);

    }
}
