package finalmethod;

/*final 修饰成员方法*/
public class Demo04FinalMethod {
    public void normalMethod() {
        System.out.println("Demo04FinalMethod normalMethod");
    }

    public final void finalMethod() {
        System.out.println("Demo4FinalMethod finalMethod");
    }

    public static void main(String[] args) {
        Demo04FinalMethod demo04FinalMethod=new Demo04FinalMethod();
        //直接调用，没有任何影响
        demo04FinalMethod.normalMethod();
        demo04FinalMethod.finalMethod();
    }
}
