package finalmethod;
/*final 修饰局部变量  局部变量：在方法里*/
public class Demo01FinalLocalVariable {
    public static void main(String[] args) {
        //局部变量
        int a =10;
        System.out.println(++a);
        //final 修饰的局部变量 只能赋值一次
        final int b=10;
       // b=20;再次赋值
        System.out.println(b);

        byte b1=57;
        b1=23+24;//字面量相加，在127的范围内

        byte b2=120;
        //b1=b2+34;//变量+字面量，不确定范围

        final  byte b3=23;//确定是最终值
        b1=b3+34;
    }
}
