package finalmethod;
/*final 修饰的成员方法的时候，子类不能重写父类的方法*/
public class Demo04FinalChild extends Demo04FinalMethod {
    @Override
    public void normalMethod() {
        super.normalMethod();
    }
/*
final 修饰的成员方法 ，子类不能重写父类的方法
    public final void finalMethod() {
    }*/
}
