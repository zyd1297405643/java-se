package finalmethod;

/*final 修饰成员变量  成员变量：类里
*final 修饰的变量都叫常量 不可修该 */
public class Demo02FinalMemberVariable {
    int a = 10;
    int b;
    final int c;/*=18*/
    ;//final修饰的成员方法必须初始化

    //  可以在构造方法中进行初始化
    {
        //c = 18;
    }

    Demo02FinalMemberVariable() {
        c = 10;
    }

    Demo02FinalMemberVariable(int a) {
        c=20;
    }

    public static void main(String[] args) {
        Demo02FinalMemberVariable demo02FinalMemberVariable=new Demo02FinalMemberVariable();
         demo02FinalMemberVariable.a++;
        System.out.println(demo02FinalMemberVariable.a);
       // final 修饰的成员变量无论如何都不能修改
        // demo02FinalMemberVariable.c++;
    }

}

