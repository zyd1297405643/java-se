package finalmethod;

import java.util.Arrays;

/*final 修饰引用类型*/
public class Demo03Variable {
    public static void main(String[] args) {
       final int [] arr=new int[]{1,2,3,4,5};
        arr[0]=10;//对象内容可以修改
        System.out.println(Arrays.toString(arr));
        // final 修饰的引用类型变量 -引用（地址值）不能变
        //不可行arr=new int[]{2,3,4};
    final Person  person=new Person();
    person.a=20;//对象中的属性可以修改
        // final 修饰的引用类型变量 -引用（地址值）不能变
        //person=new Person();
    }

}
 class Person {
    int a=10;
}