package calendar;

import java.util.Calendar;
import java.util.Date;


public class CalenderMain {
    public static void main(String[] args) {
        //使用静态方法 --获取当前系统时间
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar);
       //Date->calendar
        Date time = calendar.getTime();
        System.out.println(time);
        /* add-根据日历的规则，为给定的日历字段添加或减去指定的时间量。
           set-时间的跳转
           get-返回给定日历字段的值。
           getTime-返回一个表示此 Calendar 时间值（从历元至现在的毫秒偏移量）的 Date 对象
           setTime-使用给定的 Date 设置此 Calendar 的时间。
        */

        //get方法
        System.out.println("年:" + calendar.get(Calendar.YEAR));
        System.out.println("月:" + calendar.get(Calendar.MONTH));
        System.out.println("第几周:" + calendar.get(Calendar.WEEK_OF_MONTH));
        System.out.println("第几天:" + calendar.get(Calendar.DATE));

        //add
        calendar.add(Calendar.DATE, 15);
        System.out.println(calendar);

        //calender ->Date  getTime返回一个表示此 Calendar 时间值（从历元至现在的毫秒偏移量）的 Date 对象
        Date time1 = calendar.getTime();
        System.out.println(time1);

        //Date-->calendar
       calendar.setTime(time);
        System.out.println(calendar);

        //set-时间的跳转具体到某一天   public void set(int field,int value)
        calendar.set(Calendar.DAY_OF_MONTH, 5);
        Date time2 = calendar.getTime();
        System.out.println(time2);
    }
}
