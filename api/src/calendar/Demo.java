package calendar;

import java.util.Calendar;
import java.util.Date;

public class Demo {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        Date time = calendar.getTime();
        System.out.println("商品生产日期" + time);

        calendar.add(Calendar.DATE, 48);
        calendar.add(Calendar.DATE, -7);

        calendar.set(Calendar.DAY_OF_WEEK,Calendar.THURSDAY);
        Date time1 = calendar.getTime();
        System.out.println("促销日" + time1);

    }
}
