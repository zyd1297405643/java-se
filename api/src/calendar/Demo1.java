package calendar;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Demo1 {
    /*public static void main(String[] args) {
        SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd E a hh:mm:ss");
        *//*
        format()将给定的 Date 格式化为日期/时间字符串

        *//*
        Date date=new Date();
        System.out.println(date);

        String str=sdf1.format(date);
        System.out.println(str);
    }*/
    public static void main(String[] args) throws Exception {
        System.out.println("请输⼊出⽣⽇期 格式 yyyy-MM-dd");
        // 获取出⽣⽇期,键盘输⼊
        String birthdayString = new Scanner(System.in).next();
        // 将字符串⽇期,转成Date对象
        // 创建SimpleDateFormat对象,写⽇期模式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 调⽤⽅法parse,字符串转成⽇期对象
        Date birthdayDate = sdf.parse(birthdayString);
        // 获取今天的⽇期对象
        Date todayDate = new Date();
        // 将两个⽇期转成毫秒值,Date类的⽅法getTime
        long birthdaySecond = birthdayDate.getTime();
        long todaySecond = todayDate.getTime();
        long secone = todaySecond - birthdaySecond;
        if (secone < 0) {
            System.out.println("还没出⽣");
        } else {
            System.out.println(secone / 1000 / 60 / 60 / 24);
        }
    }
}

