package object;

import java.util.Objects;

public class ZiEquals {
    String name;
    int age;

    public ZiEquals(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        ZiEquals ziEquals = (ZiEquals) obj;
        return this.age == ziEquals.age && Objects.equals(this.name, ziEquals.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
