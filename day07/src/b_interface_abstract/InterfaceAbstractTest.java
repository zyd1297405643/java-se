package b_interface_abstract;

/*测试类
 * 3.创建实现类对象*/
public class InterfaceAbstractTest {
    public static void main(String[] args) {
        //创建实例对象
        InterfaceAbstractImplements impl = new InterfaceAbstractImplements();
        //调用抽象方法
        impl.abstractMethodA();
        impl.abstractMethodB();

        //调用静态方法，不能用实现类使用,编译错误
        //用接口名调用静态方法
        InterfaceAbstract.staticMethodC();

    }
}
