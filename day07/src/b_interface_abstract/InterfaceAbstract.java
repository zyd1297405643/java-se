package b_interface_abstract;

/*接口中：存在抽象方法
 * 接口使用步骤：
 * 1.定义接口
 */
public interface InterfaceAbstract {
    //定义抽象方法
    void abstractMethodA();


    void abstractMethodB();

    //定义一个静态方法
    static void staticMethodC() {
        System.out.println("staticMethodC");
    }
}
