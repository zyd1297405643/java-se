package b_interface_abstract;
/*接口实现类
* 2.定义实现类，实现接口 ——>implements
*/

public class InterfaceAbstractImplements implements InterfaceAbstract{
    @Override
    public void abstractMethodA() {
        System.out.println("abstractMethodA");
    }

    @Override
    public void abstractMethodB() {
        System.out.println("abstractMethodB");
    }
}
