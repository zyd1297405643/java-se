package c_interface_default;

public class InterfaceDefaultImplements implements InterfaceDefault{
    @Override
    //重写的抽象方法
    public void methodAbs1() {
        System.out.println("methodAbs1");
    }

    //默认方法可以重写或不重写(如果不重写 继承父接口的方法)

    //重写过后的默认方法，访问修饰符的权限实现类要 高于赋接口
    @Override
    public  void defaultMethod() {
        System.out.println("Default Aaa");
    }
}
