package c_interface_default;

/*接口实现类*/
public class InterfaceDefaultTest {
    public static void main(String[] args) {
        InterfaceDefaultImplements impls = new InterfaceDefaultImplements();
        //调用重写过后的抽象方法
        impls.methodAbs1();
        //可调用默认方法，实现类中如果没有这个方法，向父接口找
        impls.defaultMethod();

    }
}
