package c_interface_default;

/*接口中添加默认方法*/
public interface InterfaceDefault {
    //抽象方法
    void  methodAbs1();

    //默认方法
    public default void defaultMethod() {
        System.out.println("Default A");
    }
}
