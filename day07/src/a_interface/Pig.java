package a_interface;

public class Pig extends Animals implements InterfaceDemoB{
    @Override
    public void eat() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public void run() {

    }

    @Override
    public void m3() {

    }

    @Override
    public void m5() {

    }
}
