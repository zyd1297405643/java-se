package a_interface;

/*interface:接口 ,另一种引用类型
*  IntefaceDemoA.java-> InterfaceDemoA.class->加载*/
public interface InterfaceDemoA {
    //公开的抽象方法
    public abstract void jump();

    //  接口不是类，所以没有子类——protected 是给子类继承的
    // protected  abstract  void swin();

    //也是公开的抽象方法，默认添加public
    abstract void swing();
    //也是公开的抽象方法，默认添加abstract
    public void m1();
    //也是公开的抽象方法,开发时用
    void m2();

}
