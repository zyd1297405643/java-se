package a_interface;

public class Dog extends Animals implements InterfaceDemoA{


    @Override
    public void eat() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public void run() {

    }

    @Override
    public void jump() {

    }

    @Override
    public void swing() {

    }

    @Override
    public void m1() {

    }

    @Override
    public void m2() {

    }


}
