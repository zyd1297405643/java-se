package a_interface;

/*jdk 1.8 ->静态方法  和 默认方法
 * jdk 1.9 -> 私有方法*/
public interface InterfaceDemoB {
    //接口中的成员变量  -是final 修饰
    //public final ——>常量
    //public final static ——>静态常量
    public final static int NUM = 10;

    //jdk 1.7
    public abstract void m3();

    //jak 1.8->静态方法 和 默认方法
    public static void m4() {
        System.out.println("JDK 1.8 才支持静态方法");
    }

    public default void m5() {
        System.out.println("JDK 1.8 才支持默认方法");
    }

    //jdk 1.9 -> 私有方法
    private void m6() {
        System.out.println("JDK 1.9 才支持私有方法");
    }
}
