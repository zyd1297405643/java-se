package d_interface_private;

/*接口使用私有方法*/
public interface InterfacePrivate {
    //抽象方法
    void methodAbs();

    //默认方法
    default void methodDefaultA() {
        System.out.println("默认方法A");
       /* System.out.println("AAA");
        System.out.println("BBB");
        System.out.println("CCC");*/
        //调用私有方法
        methodPrivateA();

    }

    default void methodDefaultB() {
        System.out.println("默认方法B");
       /* System.out.println("AAA");
        System.out.println("BBB");
        System.out.println("CCC");*/
        //调用私有方法
        // -->
        methodPrivateA();
    }

    /* 普通私有方法
    1.只能在本接口中使用
         2.在本接口中的default 方法中使用
         3.实现在default 方法中代码的复用
         */
    private void methodPrivateA() {
        System.out.println("AAA");
        System.out.println("BBB");
        System.out.println("CCC");
    }


    //静态方法
    static void methodStaticA() {
        System.out.println("静态方法A");
        //不能调用普通私有方法
        //methodPrivate();
        //调用静态私有方法
        methodPrivateB();
    }

    static void methodStaticB(){
        System.out.println("静态方法B");
        methodPrivateB();
    }

    /*//静态私有方法
    * 在静态方法中不能直接调用普通成员的私有方法*/
    private static void methodPrivateB() {
        System.out.println("AAAA");
        System.out.println("BBBB");
        System.out.println("CCCC");



    }
}
