package d_interface_private;

public class InterfacePrivateTest {
    public static void main(String[] args) {
        InterfaceprivateImplemrnts impls=new InterfaceprivateImplemrnts();

        impls.methodPrivate();
        impls.methodDefaultA();
        impls.methodDefaultB();

        //用接口名调用静态方法
        InterfacePrivate.methodStaticA();
         InterfacePrivate.methodStaticB();
    }


}
