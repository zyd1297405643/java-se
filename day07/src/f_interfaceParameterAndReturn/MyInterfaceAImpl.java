package f_interfaceParameterAndReturn;
/*
    接口A 的实现类
 */
public class MyInterfaceAImpl implements MyInterfaceA{
    @Override //重写接口A中所有的抽象方法
    public void methodAbs1() {
        System.out.println("第一个抽象方法的实现");
    }

    @Override
    public void methodAbs2() {
        System.out.println("第二个抽象方法的实现");
    }
}
