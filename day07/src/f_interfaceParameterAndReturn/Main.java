package f_interfaceParameterAndReturn;

/*
    测试类 - 接口作为方法参数使用
 */
public class Main {
    public static void main(String[] args) {
        //1.创建实体类对象
        InterfaceParameterDemo demo = new InterfaceParameterDemo();
        /*
            1.没有接口 就new一个,但是接口A是不可以new对象的
                但是可以new接口的实现类 Impl
         */
        //MyInterfaceA a = new MyInterfaceA();
        //3.创建接口实现类对象
        //MyInterfaceAImpl a = new MyInterfaceAImpl();
        MyInterfaceA a = new MyInterfaceAImpl();//返回值类型是接口
        //2.调用实体类中的成员方法
        demo.method01(a);//参数需要一个接口
        //参数需要的是一个 MyInterfaceA 类型
        //实际参数: 可是是接口的实现类 对象

        //创建接口实现类对象
        MyInterfaceAImpl2 a2 = new MyInterfaceAImpl2();
        demo.method01(a2);


    }
}
