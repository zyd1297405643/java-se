package f_interfaceParameterAndReturn;
/*
    接口作为 方法参数
        此类作为实体类使用
 */
public class InterfaceParameterDemo {
    //成员变量

    //成员方法
    //接口类型的方法参数
    public void method01(MyInterfaceA a){
        //传进来a 是使用的方法是那个,具体要看传进来的是那个实现类
        // 传进来 实际上是实现类 InterfaceAImpl
        // InterfaceA a = new InterfaceAImpl();
        a.methodAbs1();//得到的是实现类中重写过后的内容
        a.methodAbs2();
        System.out.println("method01结束了");
    }
}
