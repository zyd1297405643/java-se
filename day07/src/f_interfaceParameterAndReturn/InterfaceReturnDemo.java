package f_interfaceParameterAndReturn;
/*
    接口作为返回值使用
        此类作为实体类使用
 */
public class InterfaceReturnDemo {
    //成员变量

    //成员方法 - 接口类型作为返回值类型
    public MyInterfaceA method02(){
        System.out.println("method02方法结束了");
        //需要拿到接口A中的内容 -> 接口不能new对象\
        //创建一个接口的实现类对象,->向上造型为接口类型
       /* MyInterfaceAImpl a = new MyInterfaceAImpl();
          MyInterfaceA a = new MyInterfaceAImpl();
          //接口类型的对象 返回
        return a;*/
        return new MyInterfaceAImpl();
    }

}
