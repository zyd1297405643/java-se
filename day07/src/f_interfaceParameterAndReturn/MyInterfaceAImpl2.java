package f_interfaceParameterAndReturn;
/*
    接口A的第二个实现类
 */
public class MyInterfaceAImpl2 implements MyInterfaceA{
    @Override
    public void methodAbs1() {
        System.out.println("接口A的第二个实现类中的第一个抽象方法");
    }

    @Override
    public void methodAbs2() {
        System.out.println("接口A的第二个实现类中的第二个抽象方法");
    }
}
