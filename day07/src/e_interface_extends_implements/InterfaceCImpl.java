package e_interface_extends_implements;
/*接口的实现类*/
public class InterfaceCImpl implements  InterfaceC{
    @Override
    public void methodAbsA() {

    }

    @Override
    public void methodAbsB() {

    }

    @Override
    public void methodAbsAB() {

    }
}
