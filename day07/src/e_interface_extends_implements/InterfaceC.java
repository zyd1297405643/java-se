package e_interface_extends_implements;
/*接口继承接口  多继承*/
public interface InterfaceC extends InterfaceA,InterfaceB{
    @Override//必须重写接口A和接口B中的相同的默认方法
    default void methodDefaultAB() {

    }
}
