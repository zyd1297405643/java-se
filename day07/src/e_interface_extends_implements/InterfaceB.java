package e_interface_extends_implements;

public interface InterfaceB {
    //抽象方法
    void methodAbsB();

    //默认方法
    default void methodDefaultB() {
        System.out.println("InterfaceB methodDefaultB");
    }

    //接口A 和 接口B有相同的抽象方法
    void methodAbsAB();

    //接口A 和 接口B有相同的默认方法
    default void methodDefaultAB() {
        System.out.println("InterfaceB methodDefaultAB");
    }
}
