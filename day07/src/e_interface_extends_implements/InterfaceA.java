package e_interface_extends_implements;

public interface InterfaceA {

    //抽象方法
    void methodAbsA();

    //默认方法
    default void methodDefaultA() {

        System.out.println("InterfaceA methodDefaultA");
    }

    //接口A 和 接口B有相同的抽象方法
    void methodAbsAB();

    //接口A 和 接口B有相同的默认方法
    default void methodDefaultAB() {

        System.out.println("InterfaceA methodDefaultAB");
    }

}
