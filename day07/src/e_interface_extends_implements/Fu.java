package e_interface_extends_implements;

public abstract  class Fu {
    //抽象方法
    public abstract  void methodFu();

    //普通方法
    public void normalMethod(){
        System.out.println("Fu normalMethod");
    }
}
