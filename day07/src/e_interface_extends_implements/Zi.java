package e_interface_extends_implements;
/*
    实现类: 子类
        1.一个类一定有唯一的父类,但是可以有多个父接口
        2.实现多个接口,将这些接口中所有的抽象方法都实现掉
        3.父类中如果有抽象方法,也要实现掉
        4.如果多个接口中,有重复的抽象方法,只需要实现一次即可
        5.如果多个接口中,有重复的default方法,必须要重写一个
 */
public class Zi extends Fu implements InterfaceA, InterfaceB {
    //子类还可以有自己的成员方法
    public void methodZi() {

    }

    @Override//重写父类中的抽象方法
    public void methodFu() {

    }

    @Override//重写InterfaceA中的抽象方法
    public void methodAbsA() {
        System.out.println("重写过后的Zi methodAbsA ");
    }

    @Override//重写InterfaceB中的抽象方法
    public void methodAbsB() {
        System.out.println("重写过后的Zi methodAbsB ");
    }

    @Override//重写过后的相同的抽象方法
    public void methodAbsAB() {
        System.out.println("重写过后的Zi methodAbsAB");
    }

    @Override
    public void methodDefaultAB() {
        System.out.println("重写过后的Zi methodDefaultAB");
    }


}
