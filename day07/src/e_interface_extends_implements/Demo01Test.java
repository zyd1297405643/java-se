package e_interface_extends_implements;

public class Demo01Test {
    public static void main(String[] args) {
        //创建zi对象
        Zi zi = new Zi();
        zi.methodAbsA();
        zi.methodAbsB();
        zi.methodAbsAB();
        zi.methodFu();
        zi.methodZi();
        zi.methodDefaultAB();

        InterfaceCImpl impl=new InterfaceCImpl();
        impl.methodAbsA();
        impl.methodAbsAB();
        impl.methodAbsB();
        impl.methodDefaultAB();

        /*
            zi 中可以调用的方法
                1.Fu中继承的,重写的方法
                2.所有接口中重写的,继承的方法 (default)
                3.自己类中的方法
         */
        /*
            调用重写过的默认方法,就不会往上找
                如果没有重写,只是继承过来的话,会不知道继承的是那个接口中的方法
         */


    }
}
