package stack;

import java.util.Stack;

public class StackDemo {
    public static void main(String[] args) {
        Stack stack = new Stack();
        //入栈
        stack.push("1");
        stack.push("2");
        stack.push("3");
        //出栈
        stack.pop();
        //栈顶元素
        System.out.println(stack.peek());
    }
}
