package list;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("1");
        list.add("2");
        list.add("3");
        System.out.println(list);
        //前包含后不包含
        List sub = list.subList(0, 2);
        System.out.println(sub);
        sub.add("zs");
        //原集合和字集合公用同一块内存
        System.out.println(list);
        list.add(2, "4");
        System.out.println(list);
        list.set(2, "5");
        System.out.println(list);
        list.remove(2);
        System.out.println(list);
        list.remove("1");
        System.out.println(list);
        System.out.println(list.get(2));
        List c=new ArrayList();
        c.add("10");
        list.addAll(c);
        System.out.println(list);

    }
}
