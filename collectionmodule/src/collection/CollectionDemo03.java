package collection;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionDemo03 {
    public static void main(String[] args) {
        Collection<Object> coll1 = new ArrayList<>();
        //添加
        coll1.add("张飞");
        coll1.add("刘备");
        coll1.add("关羽");
        System.out.println("coll1"+coll1);


        //创建一个集合
        Collection<Object> coll2 = new ArrayList<>();
        coll2.add("大乔");
        coll2.add("小乔");
        System.out.println("cpll2"+coll2);

        //addAll
        coll1.addAll(coll2);
        System.out.println("coll1+coll2"+coll1);

        //containsAll 检测 arraylist 是否包含指定集合中的所有元素
        Collection<Object> coll3 = new ArrayList<>();
        coll3.add("大");
        coll3.add("小");
        boolean b1 = coll1.containsAll(coll3);
        System.out.println(b1);

        //removeAll 删除 前者删除与后者相同的元素
        Collection<Object> coll4 = new ArrayList<>();
        coll4.add("大乔");
        coll4.add("小乔");
        boolean b = coll1.removeAll(coll4);
        System.out.println(b);
        System.out.println("删除coll1中的coll4后"+coll1);
    }
}
