package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorDemo {
    public static void main(String[] args) {
        // 使⽤多态⽅式 创建对象
        Collection<Object> coll = new ArrayList<Object>();
        // 添加元素到集合
        coll.add("串串星⼈");
        coll.add("吐槽星⼈");
        coll.add("汪星⼈");
        // 遍历
        //简易版迭代器
        //（元素类型type  元素变量value  ：遍历对象obj）
        for (Object o : coll) {
            System.out.println(o);
        }


        // 使⽤迭代器 遍历 每个集合对象都有⾃⼰的迭代器
        Iterator<Object> it = coll.iterator();
        // 泛型指的是 迭代出 元素的数据类型
        //public E next() ：返回迭代的下⼀个元素。
        //public boolean hasNext() ：如果仍有元素可以迭代，则返回 true
        while (it.hasNext()) { // 判断是否有迭代元素
            Object obj = it.next(); // 获取迭代出的元素
            System.out.println(obj);

            /*使用集合的remove()删除元素
             * 参数传的是Object
             * ConcurrentModificationException
             * 并发修改异常
             * 出现并发异常：同一个时间内操作同一对象*/
            //coll.remove(obj);

            /*使用迭代器remove()删除元素
             * */
            it.remove();
            System.out.println(coll);
        }


    }

}

