package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/*集合转换成数组 toArrya()*/
public class CollectionDemo05 {
    public static void main(String[] args) {
        Collection<String> coll = new ArrayList();//向上造型
        //添加
        coll.add("张三");
        coll.add("李四");
        coll.add("王五");
        // toArrya()
        // Object[] toArray()转换成⼀个Object数组
        Object[] objects = coll.toArray();
        System.out.println(Arrays.toString(objects));


        //toArray(T<>)
        /*参数：跟数组的长度无关
         * 只是需要这个参数数组类型*/
        String[] strings = coll.toArray(new String[2]);
        System.out.println(Arrays.toString(strings));
    }
}
