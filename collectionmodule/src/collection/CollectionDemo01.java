package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class CollectionDemo01 {
    public static void main(String[] args) {
        //创建Collection
        Collection<String> coll = new ArrayList();//向上造型

        //添加
        coll.add("张三");
        coll.add("李四");
        coll.add("王五");
        System.out.println(coll);

        //包含
        boolean b = coll.contains("张三");
        System.out.println(b);

       //集合大小
        System.out.println(coll.size());

        //判断集合是否为空
        boolean b1 = coll.isEmpty();
        System.out.println(b1);

        // toArrya()
        // Object[] toArray()转换成⼀个Object数组
        Object[] objects = coll.toArray();
        System.out.println(Arrays.toString(objects));

        //toArray(T<>)
        /*参数：跟数组的长度无关
        * 只是需要这个参数数组类型*/
        String[] strings = coll.toArray(new String[2]);
        System.out.println(Arrays.toString(strings));

        //删除
        coll.remove("李四");
        System.out.println(coll);

        //清楚集合
        coll.clear();
        System.out.println(coll);

        /*[张三, 李四, 王五]
true
3
false
[张三, 李四, 王五]
[张三, 李四, 王五]
[张三, 王五]
[]*/

    }
}
