package collection;

import java.util.Arrays;
import java.util.Collection;

/*数组转换成集合  Arrays.asList() */
public class CollectionDemo04 {
    public static void main(String[] args) {
        String[] str = {"lucy", "tom", "hh"};
        System.out.println(Arrays.toString(str));
        //数组类工具类 Arrays
        Collection<String> coll = Arrays.asList(str);
        System.out.println(coll);

        str[0] = "tony";
        /*数组和集合使用同一块内存空间*/
        System.out.println(coll);//[tony, tom, hh]

        /*
         //UnsupportedOperationException  不支持的操作异常
        coll.add("li");
        System.out.println(coll);*/

    }
}
