package generic_paradigm;

public class GenericClassDemo {
    public static void main(String[] args) {
       //创建一个泛型为String的类
        MyGenericClass<String> my=new MyGenericClass<>();
        //调用setE
        my.setE("abcd");
        //调用getE
        String s= my.getE();

        System.out.println(s);

        //创建一个泛型为Integer的类
        MyGenericClass<Integer> my1=new MyGenericClass();
        my1.setE(123);
        Integer i= my1.getE();
        System.out.println(i);
    }
}
