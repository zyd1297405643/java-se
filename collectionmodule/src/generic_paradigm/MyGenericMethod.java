package generic_paradigm;

/*修饰符 <代表泛型的变量> 返回值类型 ⽅法名(参数){ }*/
public class MyGenericMethod {
    public <E> void show(E e) {
        System.out.println(e.getClass());
    }

    public <E> E show2(E e) {
        return e;
    }
}
