package generic_paradigm;

/*按照斗地主的规则，完成洗牌发牌的动作
 * 具体规则：
 * 使用54张牌打乱顺序，3个玩家参与游戏
 * 三人交替摸排，每人17张牌，最后三张作为底牌
 *  跟数组有关的工具类 ——Arrays
 * 跟Collection 集合相关的工具类
 *  collections.shuffer()  打乱顺序
 * */

import java.util.ArrayList;
import java.util.Collections;

public class Poker {
    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList();
        ArrayList<String> nums = new ArrayList();
        ArrayList<String> cards = new ArrayList();
        ArrayList<String> boss = new ArrayList<>();
        ArrayList<String> players1 = new ArrayList<>();
        ArrayList<String> players2 = new ArrayList<>();
        ArrayList<String> players3 = new ArrayList<>();
        ArrayList<String> dipai = new ArrayList<>();

        colors.add("♠");
        colors.add("♥");
        colors.add("■");
        colors.add("♣");

        for (int i = 2; i <= 10; i++) {
            nums.add(i + "");
        }
        nums.add("J");
        nums.add("Q");
        nums.add("K");
        nums.add("A");
        boss.add("大\uD83C\uDCCF");
        boss.add("小\uD83C\uDCCF");
        for (String num : nums) {
            for (String color : colors) {
                cards.add(num + color);
            }
        }
        for (String bos : boss) {
            cards.add(bos);
        }

        //打乱牌的顺序
        Collections.shuffle(cards);

        for (int i = 0; i < cards.size(); i++) {
            if (i >= cards.size()-3) {
                dipai.add(cards.get(i));
            } else if (i % 3 == 0) {
                players1.add(cards.get(i));
            } else if (i % 3 == 1) {
                players2.add(cards.get(i));
            } else
                players3.add(cards.get(i));
        }

        System.out.println(players1);
        System.out.println(players2);
        System.out.println(players3);
        System.out.println(dipai);
    }

}
