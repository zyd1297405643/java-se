package generic_paradigm;
/*修饰符 class 类名<代表泛型的变量> { }
* class ArrayList<E> {
 public boolean add(E e){ }
 public E get(int index){ }
 ....
}*/
public class MyGenericClass<E> {

    // 没有E类型，在这⾥代表 未知的⼀种数据类型 未来传递什么就是什么类型
    private E e;

    public void setE(E e) {
        this.e = e;
    }

    public E getE() {
        return e;
    }

}
