package generic_paradigm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class GenericDemo01 {

        public static void main(String[] args) {
            Collection<String> list = new ArrayList<String>();
            list.add("hello");
            list.add("bonnie");
            // list.add(5); // 当集合明确类型后，存放类型不⼀致就会编译报错
            // 集合已经明确具体存放的元素类型，那么在使⽤迭代器的时候，迭代器也同样会知道
            //具体遍历元素类型
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                String str = it.next();
                // 当使⽤Iterator<String>控制元素类型后，就不需要强转了。获取到的元素直接就
               // 是String类型
                System.out.println(str);
            }
        }
    }
