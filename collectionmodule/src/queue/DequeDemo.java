package queue;

import java.util.Deque;
import java.util.LinkedList;

public class DequeDemo {
    public static void main(String[] args) {
        //双端对列
        Deque q=new LinkedList();
        q.offer("z");
        q.offer("e");
        q.offerLast("23");
        q.offerFirst("2");
        System.out.println(q);
    }
}
