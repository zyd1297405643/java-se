package set;

import java.util.*;

/*
 * 数组排序 Arrays.sort
 * List排序  Collections.sort
 *  Set排序  TreeSet
 * Map 排序   TreeMap*/
public class Sort {
    public static void main(String[] args) {
        //不能使用基本数据类型
        Integer[] arr = new Integer[]{1, 2, 5, 3, 0,};
        Arrays.sort(arr, new Comparator<Integer>() {
            @Override
            //重写compare方法
            public int compare(Integer o1, Integer o2) {
                //降序
                return o2 - o1;
            }
        });
        System.out.println(Arrays.toString(arr));


        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(4);
        list.add(3);
        list.add(2);
        //重写Collections.sort
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
                //o2-o1 倒序
                //o1-o2 正序
            }
        });
        System.out.println(list);

        TreeSet<Integer> treeSet = new TreeSet<>(new Comparator<Integer>() {
            @Override
            //自定义排序
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        treeSet.add(8);
        treeSet.add(7);
        treeSet.add(11);
        treeSet.add(4);
        System.out.println(treeSet);


    }


}
