package set;

import java.util.HashSet;
/*  A:存入集合的顺序和取出集合的顺序不一致
       B:没有索引
       C:存入集合的元素没有重复
       存储位置不是真正的随机位置，根据HashCode方法计算(决定)最终位置*/

public class SetDemo {
    public static void main(String[] args) {
        HashSet hs=new HashSet();
        hs.add("4");
        hs.add("2");
        hs.add("3");
        hs.add("3");
        System.out.println("3".hashCode()=="4".hashCode());
        System.out.println("4".hashCode());
        System.out.println(hs);
    }
}
