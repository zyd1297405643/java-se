package set;

import java.util.HashSet;

public class HashSetDemo {
    public static void main(String[] args) {
        HashSet<User> hs=new HashSet();
        User user1=new User(18,"张三");
        User user2=new User(18,"李四");
        User user3=new User(19,"张三");
        User user4=new User(19,"李四");
        hs.add(user1);
        hs.add(user2);
        hs.add(user3);
        hs.add(user4);
        System.out.println(hs);
    }
}
