package set;

import java.util.TreeSet;

/*TreeSet 中序遍历*/
public class TreeSetDemo {
    public static void main(String[] args) {
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(8);
        treeSet.add(7);
        treeSet.add(11);
        treeSet.add(4);
        System.out.println(treeSet);


       // 如果没有Comparable接口则显示ClassCastException 类型转换异常
        TreeSet<User> ts=new TreeSet<>();
        User user = new User(18,"zs");
        User user1 = new User(20,"ls");
        ts.add(user);
        ts.add(user1);
        System.out.println(ts);
    }
}
