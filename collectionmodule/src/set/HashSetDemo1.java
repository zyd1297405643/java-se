package set;

import java.util.HashSet;

public class HashSetDemo1 {
    public static void main(String[] args) {
        HashSet<User> hs=new HashSet();
        User user1=new User(18,"张三");
        User user2=new User(19,"李四");
        hs.add(user1);
        hs.add(user2);

        System.out.println(hs);
        //在同一对象上多次调用 hashCode 方法时，
        //必须一致地返回相同的整数，前提是对象上 equals 比较中所用的信息没有被修改。
        //修改user2年龄再删除
        System.out.println(user2.hashCode());
        user2.setAge(20);
        System.out.println(user2.hashCode());
        //删除user2
        hs.remove(user2);//无法删除
        System.out.println(hs);

        HashSet newhs=new HashSet(hs);
        System.out.println(newhs);

    }
}
