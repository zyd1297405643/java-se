package arrayList;

import java.util.ArrayList;

public class ArrayListDemo {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("1");
        arrayList.add("2");
        ArrayList<String> arrayList1 = new ArrayList<>(arrayList);
        arrayList1.add("3");
        arrayList1.add("4");
        System.out.println(arrayList1);
    }
}
