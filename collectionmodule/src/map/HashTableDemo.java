package map;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class HashTableDemo {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put(null, 90);
        map.put("语文", null);
        map.put("科学", 92);
        map.put("英语", null);
        map.put("地理", 94);
        System.out.println(map);

        Hashtable<String, Integer> ht = new Hashtable<>();
        //key存在null的情况
        //NullPointerException  key不允许存在null
        ht.put(null, 90);
        System.out.println(ht);
        //value存在null的情况
        //NullPointerException   value不允许存在null
        ht.put("数学", null);
        System.out.println(ht);


    }
}
