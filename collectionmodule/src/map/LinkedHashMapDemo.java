package map;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/*有序插入*/
public class LinkedHashMapDemo {
    public static void main(String[] args) {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("邓超", "孙俪");
        map.put("⻩晓明", "杨颖");
        map.put("刘德华", "朱丽倩");

        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}
