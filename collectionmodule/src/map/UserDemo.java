package map;

import java.util.HashMap;

public class UserDemo {
    public static void main(String[] args) {
        HashMap<String, User > hs = new HashMap<>();
        hs.put("北京",new User(18,"小乔"));
        hs.put("上海",new User(19,"周瑜"));
        hs.put("深圳",new User(18,"孙策"));
        hs.put("北京",new User(18,"张三"));
        System.out.println(hs);
        HashMap<User, String > hs1 = new HashMap<>();
        hs1.put(new User(18,"小乔"),"北京");
        hs1.put(new User(19,"周瑜"),"上海");
        hs1.put(new User(18,"孙策"),"深圳");
        hs1.put(new User(18,"小乔"),"北京");
        System.out.println(hs1);
    }
}
