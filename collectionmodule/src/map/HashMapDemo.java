package map;

import java.util.HashMap;

public class HashMapDemo {
    public static void main(String[] args) {
        HashMap<String, Integer> hs = new HashMap<>();
        Integer v1 = hs.put("小乔", 18);
        System.out.println("v1:" + v1); //v1:null
        Integer v2 = hs.put("小乔", 13);
        System.out.println("v2:" + v2);//v2:18
        Integer v3 = hs.put("小乔", 20);
        System.out.println("v3:" + v3);
        hs.put("周瑜", 12);
        hs.put("孙策", 19);
        hs.put(null, 20);
        System.out.println(hs);
        System.out.println("--------------");
        //remove
        hs.remove("周瑜");
        System.out.println(hs);
        Integer v4 = hs.remove("张三");
        System.out.println("v4:" + v4);
        System.out.println(hs);
        System.out.println("-----------------");
        //get
        System.out.println(hs.get("孙策"));
        //当不存在key值是
        Integer v5 = hs.get("刘备");
        System.out.println("v5:" + v5);
        System.out.println("------------------");
        //containsKey(Object key)
        System.out.println(hs.containsKey("孙策"));
        System.out.println("------------------");
        //containsValue
        System.out.println(hs.containsValue(19));
        System.out.println("----------------");
        //putAll(Map<? extends k,? extends V> m)
        HashMap<String, Integer> hs1 = new HashMap<>();
        hs1.put("马超", 2);
        hs1.put("赵云", 4);
        hs.putAll(hs1);
        System.out.println(hs);
    }
}
