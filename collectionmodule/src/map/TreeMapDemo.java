package map;

import java.util.Comparator;
import java.util.TreeMap;

/*自然排序
   Comparetable ->重写compareTo()
自定义排序
  Comparator -> 重写compare()
* */
public class TreeMapDemo {
    public static void main(String[] args) {
        //自然排序
        TreeMap<String, Integer> map = new TreeMap<>();
        map.put("数学", 90);
        map.put("语文", 91);
        map.put("科学", 92);
        map.put("英语", 93);
        map.put("地理", 94);
        System.out.println("地理的hashCode():"+"地理".hashCode());
        System.out.println("数学的hashCode():"+"数学".hashCode());
        System.out.println("科学的hashCode():"+"科学".hashCode());

        System.out.println(map);
        //自定义排序
        TreeMap<String, Integer> tm = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        tm.put("X", 10);
        tm.put("Z", 11);
        tm.put("Q", 12);
        System.out.println("X的hashCode():"+"X".hashCode());
        System.out.println("Z的hashCode():"+"Z".hashCode());
        System.out.println("Q的hashCode():"+"Q".hashCode());

        System.out.println(tm);
    }
}
