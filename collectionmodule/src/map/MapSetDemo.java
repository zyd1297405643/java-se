package map;

import java.util.*;

public class MapSetDemo {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("数学", 90);
        map.put("语文", 91);
        map.put("科学", 92);
        map.put("英语", 93);
        map.put("地理", 94);
        System.out.println(map);
        //iterator()迭代器遍历
        Set<String> set = map.keySet();
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String s = it.next();
            System.out.println(s);
        }
        System.out.println("----------------");
        // Set<K> keySet()
        // 返回此映射中包含的键的 Set 视图。
        Set<String> setKey = map.keySet();
        for (String key : setKey //map.ketSet()
        ) {
            System.out.println(key);
        }
        System.out.println("---------------------");
        // Set<Map.Entry<K,V>> entrySet()
        // 返回此映射中包含的映射关系的 Set 视图。
        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
        for (Map.Entry<String, Integer> entry : entrySet
        ) {
            System.out.println(entry);
        }
        System.out.println("------------------");
        // Collection<V> values()
        //  返回此映射中包含的值的 Collection 视图。
        Collection<Integer> c = map.values();
        for (Integer i : c
        ) {
            System.out.println(i);
        }

    }
}
