package linkedlist;

import java.util.LinkedList;

public class LinkedListDemo {
    public static void main(String[] args) {
        LinkedList list=new LinkedList();
        list.add("1");
        list.addFirst("2");
        list.addLast("3");
        System.out.println(list.get(1));
        list.removeFirst();
        list.removeLast();
        System.out.println(list);
    }
}
