package jdk;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Jdk1_9Demo {
    public static void main(String[] args) {
        Set<String> str1 = Set.of("a", "b", "c");
        // str1.add("c"); 这⾥编译的时候不会错，但是执⾏的时候会报错，因为是不可变的
        System.out.println(str1);
        Map<String, Integer> str2 = Map.of("a", 1, "b", 2);
        System.out.println(str2);
        List<String> str3 = List.of("a", "b");
        System.out.println(str3);
    }
}
