package rewrite;
//重写父类方法：  方法类型，方法名和父类完全一致  super 继承了父类的方法
public class Pig extends Animal {
    String name;//默认值null
    public void eat(){
        System.out.print("猪用鼻子拱  ");
        super.eat();
    }
    public void show(String name)
    {
        super.show(name);
        int age=18;
        System.out.println(age);
    }
}
