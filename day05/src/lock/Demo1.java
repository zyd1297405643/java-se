package lock;

public class Demo1 {
    public static void main(String[] args) {
        Circular[] circular = new Circular[10];
        for (int i = 0; i < circular.length; i++) {
            circular[i]=new Circular(Math.random()*15+5);
            System.out.println(circular[i].area());
        }
    }
}

class Circular {
    double Radius;
    double Pi = 3.14;

    //构造器
    public Circular(double Radius) {
        this.Radius = Radius;
    }

    public double area() {

        return Pi * Radius * Radius;
    }

    public double perimeter() {

        return 2 * Pi * Radius;
    }
}
