
package hotel;

/* 酒店管理系统
  a.酒店类: 房间二维数组
           方法(功能): 入住(房号, 客人的姓名)
                      退房(房号)
                       查询() -> 打印房间的状态
  b.房间类: 房号(1001), 客人
  c.客人类: 姓名

  房间的状态
  1001 1002 1003 1004...
  空   tom   空   李四
  2001 2002 2003 2004
  空   空    空   空
  ...
*/
public class Hotel {
    Room[][] rooms = new Room[3][4];//引用数组 要给数组的元素初始化

    public Hotel() {
//        给room 的元素 初始化
        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < rooms[i].length; j++) {
                int roomNum = (i + 1) * 1000 + j + 1;
                rooms[i][j] = new Room(roomNum);
            }
        }
    }

    //   入住，房号，客人姓名
    public void checkIn(int roomNum, String customerName) {
        //输入房间号和客户名字
        int i = roomNum / 1000 - 1;
        int j = roomNum % 1000 - 1;
        if (rooms[i][j].customer != null) {
            System.out.println("入住失败,该房间已经有人");
        } else {
            rooms[i][j].customer = new Customer(customerName);
            System.out.println(customerName + "入住成功" + roomNum + "号房");
        }
    }

    //  退房 ，房号
    public void checkOut(int roomNum) {
        int i = roomNum / 1000 - 1;
        int j = roomNum % 1000 - 1;
        if (rooms[i][j].customer == null) {
            System.out.println(+roomNum + "号房间退房失败,该房间没有人");
        } else {
            //获得退房客人名字
            String name;
            name = rooms[i][j].customer.name;
            rooms[i][j].customer = null;
            System.out.println(+roomNum + "号房间退房成功,欢迎" + name + "下次再来");
        }

    }


    //查询
    public void check() {
        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < rooms[i].length; j++) {
                if(j%4==0)
                    System.out.println();
                if (rooms[i][j].customer == null) {
                    System.out.print("房间号:" + rooms[i][j].roomNum + "\t没有客户入住" +"\t");
                } else
                    System.out.print("房间号:" + rooms[i][j].roomNum+ "\t客户信息为" + rooms[i][j].customer.name+ "\t");
            }

        }


    }
}

