package hotel;

import java.util.Scanner;

//欢迎进入指正酒店管理系统 new 酒店()
//请输入指令：查询（check） 入住（checkin） 退房（checkout）退出系统（exit）
/*check   显示差查询的房间状态
 * checkin   请输入入住的房号和入住的姓名  1001 李四 入住成功/该房间有人
 * checkout  请输入退房号   1001   退房成功，欢迎李四再来/该房间没有入住信息
 * exit 退出系统*/

public class Main {
    public static void main(String[] args) {
        Hotel hotel = new Hotel();
        int roomNum;
        String name;
        System.out.println("欢迎入住指针酒店,共3层每层4间，格式1001~1004,2001~2004,...");
        while (true) {
            System.out.println("输入指令:查询(check) 入住(checkin) 退房(checkout)退出系统(exit)");
            Scanner sc = new Scanner(System.in);
            String instructions = sc.next();
            if ("check".equals(instructions) || "checkin".equals(instructions)
                    || "checkout".equals(instructions) || "exit".equals(instructions)) {
                switch (instructions) {
                    case "check":
                        hotel.check();
                        System.out.println();
                        break;
                    case "checkin":
                        System.out.println("输入要入住的房间号和入住者名字");
                        roomNum = sc.nextInt();
                        name = sc.next();
                        hotel.checkIn(roomNum, name);
                        break;
                    case "checkout":
                        System.out.println("输入要退的房间号");
                        roomNum = sc.nextInt();
                        hotel.checkOut(roomNum);
                        break;
                    case "exit":
                        System.out.println("谢谢光临");
                        System.exit(0);
                }
            }
            else
                System.out.println("指令错误");
        }

    }
}


