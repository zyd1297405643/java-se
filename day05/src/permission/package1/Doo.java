package permission.package1;

/**
 * Doo和Foo是同包,并且也是父子关系
 */
public class Doo extends Foo {
    public void fun1() {
        this.m1();
        this.m2();
        this.m3();
//        this.m4();
    }
}
