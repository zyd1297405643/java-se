package permission.package1;

/**
 * Koo 和 Foo 是同包, 但不是父子
 */
public class Koo {
    public static void main(String[] args) {
        Foo foo = new Foo();
        foo.m1(); // public
        foo.m2(); // protected
        foo.m3(); // default,不填
//       foo.m4();
    }
}
