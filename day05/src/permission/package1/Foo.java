package permission.package1;

public class Foo {
    public int age;
    protected int name;
    int score;
    private int sex;
    public Foo() {

    }
    public void m1() {
        System.out.println("public m1");
    }
    protected void m2() {
        System.out.println("protected m2");
    }
    void m3() {
        System.out.println("默认的 m3");
    }
    private void m4() {
        System.out.println("private m4");
    }

//同类
    public void fun1() {
        this.m1();
        this.m2();
        this.m3();
        this.m4();
    }
}
