package permission.package2;

import permission.package1.Foo;

/**
 * Too和Foo不同包,但是是父子关系
 */
public class Too extends Foo {
    public void fun1() {
        this.m1(); // public
        this.m2(); // protected
//        this.m3();
//        this.m4();
    }
}
