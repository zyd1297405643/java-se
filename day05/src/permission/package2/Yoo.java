package permission.package2;

import permission.package1.Foo;
//import permission.package1.Poo;

/**
 * Yoo 和 Foo 没有任何关系(不同包,也不是父子)
 */
public class Yoo {
    public static void main(String[] args) {
        Foo foo = new Foo();
        foo.m1(); // public
//        foo.m2();
//        foo.m3();
//        foo.m4();
//        new Poo();
    }
}
