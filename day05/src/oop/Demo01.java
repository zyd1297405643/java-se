package oop;

public class Demo01 {
    /*定义一个int[] 类型的数组a,长度是5
    * 其中第3个元素有4个元素，并且其中的第四个元素是3
    * 定义一个int[] 类型的数组吧，长度是6
    * 其中第4个元素的第二个元素是8，总共有>=2个元素
    * 定义一个int[] 类型的数组c，长度是3
    * 每个元素，都有4个元素，并且值都是10*/
    public static void main(String[] args) {
        int[][] a=new  int[5][];
        a[2]=new int[4];
        a[2][3]=3;

        int[][] b=new int[6][];//数组（引用类型）默认值是null
        b[3]=new int[2];//数组元素初始化
        b[3][1]=8;

        int[][] c= new int[3][4];
        for(int i=0;i< c.length;i++) {
            for (int j = 0; j < c[i].length; j++){
                c[i][j]=10;
                System.out.println(c[i][j]);
            }
        }

    }
}
