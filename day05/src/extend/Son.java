package extend;
//子类可以继承父类的成员变量和成员方法
//对于父类构造方法子类得调用 super()
public class Son extends Father {
double score;
    public   Son()
    {
        super("qq");

    }
    public Son(String name,int age,double score){
        //调用父类构造方法
        super("abcd",18);
        this.score=score;

    }

    public void study()
    {
        System.out.println(name+"正在学习");
        //继承父类方法
        sleep();
        area();
    }

    @Override
    public void area() {
        super.area();
    }
}
