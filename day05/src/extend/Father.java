package extend;

public class Father {
    int age;
    String name;

    //无参构造方法
    public Father() {
        super();
    }

    public Father(String name, int age) {
        this.name = name;
    }

    public Father(String name) {
       this.name=name;
    }

    //方法
    public void sleep() {
        System.out.println("睡了");
    }

    public void area() {

    }

}
