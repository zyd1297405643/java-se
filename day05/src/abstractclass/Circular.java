package abstractclass;
//如果抽象类的子类不是抽象类，那么子类必须实现父类中所有的抽象方法
public abstract class Circular extends Shape {

    double Radius;
    double Pi = 3.14;

    //构造器
    public Circular(double Radius) {
        this.Radius = Radius;
    }

    public double area() {
        return Pi * Radius * Radius;
    }

    public double perimeter() {

        return 2 * Pi * Radius;
    }

}

