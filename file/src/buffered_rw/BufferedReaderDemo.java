package buffered_rw;

import org.junit.Test;

import java.io.*;

/*BufferedReader： public String readLine() ：读⼀⾏⽂字。
BufferedWriter： public void newLine() ：写⼀⾏⾏分隔符,由系统属性定义符号*/
public class BufferedReaderDemo {
    @Test
    public void test() throws IOException {
        // 创建流对象
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("note.txt")));
        // 定义字符串,保存读取的⼀⾏⽂字
        String line = null;
        // 循环读取,读取到最后返回null
        while ((line = br.readLine()) != null) {
            System.out.print(line);
            System.out.println("------");
        }
        // 释放资源
        br.close();
    }

    @Test
    public void test1() throws IOException {

        // 创建流对象
        BufferedWriter bw = new BufferedWriter(new FileWriter("bufferedReaderDemo_out.txt"));
        // 写出数据
        bw.write("Java");
        // 写出换⾏
        bw.newLine();
        bw.write("程序");
        bw.newLine();
        bw.write("员");
        bw.newLine();
        // 释放资源
        bw.close();
    }

}