package buffered_rw;

import org.junit.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*1. 逐⾏读取⽂本信息。
2. 解析⽂本信息到集合中。
3. 遍历集合，按顺序，写出⽂本信息。*/
public class BufferedTest {
    @Test
    public void test() throws IOException {
        // 创建map集合,保存⽂本数据,键为序号,值为⽂字
        HashMap<String, String> lineMap = new HashMap<>();
        //创建流对象
        BufferedReader br = new BufferedReader(new FileReader("BufferedTest_In.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("BufferedTest_Out.txt"));
        //读取数据
        String line = null;
        //BufferedReader： public String readLine() ：读⼀⾏⽂字。
        while ((line = br.readLine()) != null) {
            // 解析⽂本
            String[] split = line.split("\\.");
            // 保存到集合
            lineMap.put(split[0], split[1]);
        }
        //释放资源
        br.close();
        //遍历集合
        Set<Map.Entry<String, String>> entrySet = lineMap.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            String key = entry.getKey();
            String value = entry.getValue();
            //写出拼接文本
            bw.write(key + "." + value);
            //写出换行
            bw.newLine();
        }
       /* for (int i = 1; i <= lineMap.size(); i++) {
            String key = String.valueOf(i);
            // 获取map中⽂本
            String value = lineMap.get(key);
            // 写出拼接⽂本
            bw.write(key + "." + value);
            // 写出换⾏
            bw.newLine();
        }*/
        bw.close();

    }


}
