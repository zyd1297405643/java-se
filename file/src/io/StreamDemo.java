package io;

import org.junit.Test;

import java.io.*;

/*utf——8 1字符=3字节
 * gbk   1字符=2字节*/
public class StreamDemo {
    @Test
    public void output() throws IOException {
        File file = new File("steramfileDemo.txt");
        //写文件 output
        FileOutputStream os = new FileOutputStream(file);
        String input = "asd";
        byte[] bytes = input.getBytes();
        os.write(bytes);
        os.write(48);
        os.write(49);
    }

    @Test
    public void input() throws IOException {
        File file = new File("steramfileDemo.txt");
        //读文件 -Input
        FileInputStream is = new FileInputStream(file);
        int read1 = is.read();
        int read2 = is.read();
        int read3 = is.read();
        int read4 = is.read();
        int read5 = is.read();

        System.out.println(read1);
        System.out.println(read2);
        System.out.println(read3);
        System.out.println(read4);
        System.out.println(read5);

    }

    //遍历
    @Test
    public void readForeach() throws IOException {
        File file = new File("steramfileDemo.txt");
        //读文件 -Input
        InputStream is = new FileInputStream(file);
        int i;
        while ((i = is.read()) != -1) {
            System.out.println(i);
        }
    }
}
