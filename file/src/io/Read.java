package io;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/*read(byte[] b, int off, int len)
          从此输入流中将最多 len 个字节的数据读入一个 byte 数组中。*/
public class Read {
    @Test
    public void test() throws IOException {

        FileOutputStream fos = new FileOutputStream("RWDemo_a.txt");
        String str = "你好,你吃饭了吗?"; //utf-8 一个中文等于三个字节，中文标点占三个字节。
        //一个英文字符等于一个字节，英文标点占一个字节。

        //将字符串->字符数组
        byte[] b = str.getBytes();
        fos.write(b);
        System.out.println(Arrays.toString(b));
        //[-28, -67, -96, -27, -91, -67, 44, -28, -67,
        // -96, -27, -112, -125, -23, -91, -83, -28, -70, -122, -27, -112, -105, 63]

    }

    @Test
    public void test1() throws IOException {
        FileInputStream fis = new FileInputStream("RWDemo_a.txt");
        byte[] bytes = new byte[25];
        int i = fis.read(bytes);
        System.out.println("第一次读i字节数:" + i);//读了23个字节
        System.out.println(Arrays.toString(bytes));
        //[-28, -67, -96, -27, -91, -67, 44, -28, -67,
        // -96, -27, -112, -125, -23, -91, -83, -28, -70, -122, -27, -112, -105, 63,0,0]

        i = fis.read(bytes);
        System.out.println("都二次读字节数i=" + i); //i=-1
        System.out.println(Arrays.toString(bytes));
    }

    @Test
    public void test2() throws IOException {
        FileInputStream fis = new FileInputStream("RWDemo_a.txt");
        byte[] bytes = new byte[5];
        int i;
        i = fis.read(bytes);
        System.out.println("第一轮i1=" + i);
        System.out.println(Arrays.toString(bytes));
        //[-28, -67, -96, -27, -91]
        i = fis.read(bytes);
        System.out.println("第2轮i2=" + i);
        System.out.println(Arrays.toString(bytes));
        //[-67, 44, -28, -67, -96]
        i = fis.read(bytes);
        System.out.println("第3轮i3=" + i);
        System.out.println(Arrays.toString(bytes));
        //[-27, -112, -125, -23, -91]
        i = fis.read(bytes);
        System.out.println("第4轮i4=" + i);
        System.out.println(Arrays.toString(bytes));
        //[-83, -28, -70, -122, -27]
        i = fis.read(bytes);
        System.out.println("第5轮i5=" + i);
        System.out.println(Arrays.toString(bytes));
        //[-112, -105, 63, -122, -27]


    }
}
