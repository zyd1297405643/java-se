package io;

import org.junit.Test;

import java.io.*;

/*文件的复制
 * 做文件的复制，意味着本身就已存在一个文件
 * 读出来的内容，要写入到另一个文件中*/
public class CopyFile {
    @Test
    public void copy() throws IOException {

        FileInputStream fis = new FileInputStream("CopyFile_fileDemo.txt");
        FileOutputStream fos = new FileOutputStream("CopyFile_copyDemo1.txt");
        int i;
        while ((i = fis.read()) != -1) {
            fos.write(i);
        }

    }
}
