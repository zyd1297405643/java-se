package io;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {
    @Test
    public void test() throws IOException {
        FileInputStream fis = new FileInputStream("CopyFile_fileDemo.txt");
        FileOutputStream fos = new FileOutputStream("CopyFile_copyDemo.txt");
        byte[] bytes = new byte[5];
        int len;
        while((len=fis.read(bytes))!=-1){
            //写入文件 截住读入的空字节
            fos.write(bytes,0,len);
        }
    }
}
