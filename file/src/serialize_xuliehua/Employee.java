package serialize_xuliehua;

import java.io.Serializable;

/* 对象-->字节*/
/*1. ⼀个对象要想序列化，必须满⾜两个条件：
该类必须实现 java.io.Serializable 接⼝， Serializable 是⼀个标记接⼝，不实现此接⼝
的类将不会使任何状态序列化或反序列化，会抛出 NotSerializableException 。
该类的所有属性必须是可序列化的。如果有⼀个属性不需要可序列化的，则该属性必须注明
是瞬态的，使⽤ transient 关键字修饰。*/
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L; //序列版本号 唯一标识
    public String name;
    public String address;
    public String sex;
    public transient int age; // transient瞬态修饰成员,不会被序列化

    public void addressCheck() {
        System.out.println("Address check : " + name + " -- " + address);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                '}';
    }
}
