package serialize_xuliehua;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
/*序列化*/
/*public final void writeObject (Object obj) ：将指定的对象写出。*/
public class SerializeDemo {
    @Test
    public void test() throws IOException {
        FileOutputStream fileOut = new FileOutputStream("employee.txt");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        Employee e = new Employee();
        e.address = "杭州";
        e.name = "张三";
        e.age = 20;
        //写出对象
        out.writeObject(e);
        out.close();
        fileOut.close();
        System.out.println("Serialized data is save");

    }

}
