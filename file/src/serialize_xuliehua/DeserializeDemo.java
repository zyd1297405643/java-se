package serialize_xuliehua;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/*public ObjectInputStream(InputStream in) ：创建⼀个指定InputStream的
ObjectInputStream。*/
public class DeserializeDemo {
    @Test
    public void test() throws IOException, ClassNotFoundException {
        Employee e =null;
        //创建反序列化流
        FileInputStream fis = new FileInputStream("employee.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        //读取对象  public final Object readObject () ：读取⼀个对象
        e= (Employee) ois.readObject();
       //释放资源
        ois.close();
        fis.close();
       /* System.out.println("Name: " + e.name); // 张三
        System.out.println("Address: " + e.address); // 杭州
        System.out.println("age: " + e.age); // 0*/
        System.out.println(e);
    }
}
