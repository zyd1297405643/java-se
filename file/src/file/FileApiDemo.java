package file;

import org.junit.Test;

import java.io.File;

public class FileApiDemo {
    @Test
    public void testPath() {

        File file = new File("a.txt");
        //获得a.txt文件的路径
        /*D:\JavaSE\file\a.txt
         * 使用new File() 获取的文件如果要存在，只能在模块下*/

        //exists()判断文件或者目录是否存在
        System.out.println(file.exists());
        //isFile() 判断是否为文件
        System.out.println(file.isFile());
          //getAbsolutePath() 获得绝对路径
        String path = file.getAbsolutePath();
        System.out.println(path);

        File file1 = new File("note.txt");
        String path1 = file1.getAbsolutePath();
        System.out.println(path1);
        System.out.println("note.txt" +file1.exists());
        System.out.println("note.txt" +file1.isFile());

        //isDirectory()此File表示的是否为⽬录。
        File file2 = new File("D:\\JavaSe");
        System.out.println("D:\\JavaSe?" + file2.exists());
        System.out.println("D:\\JavaSe?" + file2.isDirectory());
        //返回由此File表示的⽂件或⽬录的名称。
        System.out.println("D:\\JavaSe name:" + file2.getName());
        System.out.println("D:\\JavaSe length:" + file2.length());
    }

}
