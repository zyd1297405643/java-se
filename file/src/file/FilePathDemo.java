package file;

import org.junit.Test;

public class FilePathDemo {
    @Test
    public void test() {
        /*文件必须存在
        相对路径：aa.txt
        * 参考路径：当前类所在的编译目录（同一包下）*/
        String path = FilePathDemo.class.getResource("FilePathDemo_a.txt").getPath();
        System.out.println(path);
    }

    @Test
    public void test1() {
        /*文件必须存在src目录下*/
        String path = FilePathDemo.class.getClassLoader().getResource("FilePathDemo_b.txt").getPath();
        System.out.println(path);
    }
}
