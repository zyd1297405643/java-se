package file;

import java.io.File;

/*public String[] list() ：返回⼀个String数组，表示该File⽬录中的所有⼦⽂件或⽬录。
public File[] listFiles() ：返回⼀个File数组，表示该File⽬录中的所有的⼦⽂件或⽬录*/
public class FileFor {
    public static void main(String[] args) {
          //当前模块下
        File dir = new File("file");
        // 获取当前⽬录下的⽂件以及⽂件夹的名称。
        String[] names = dir.list();

        for (String name : names) {
            System.out.println(name);
        }
        // 获取当前⽬录下的⽂件以及⽂件夹对象，只要拿到了⽂件对象，那么就可以获取更多
        File[] files = dir.listFiles();
        for (File file : files) {
            System.out.println(file);
        }
    }
}
