package file;

import org.junit.Test;

import java.io.File;

public class FileDemo {
    @Test
    public void test() {
        // ⽂件路径名
        String pathname = "D:\\aaa.txt";
        File file1 = new File(pathname);
        System.out.println(file1);
        // ⽂件路径名
        String pathname2 = "D:\\aaa\\bbb.txt";
        File file2 = new File(pathname2);

        // 通过⽗路径和⼦路径字符串
        String parent = "d:\\aaa";
        String child = "bbb.txt";
        File file3 = new File(parent, child);
        // 通过⽗级File对象和⼦路径字符串
        File parentDir = new File("d:\\aaa");
        File file4 = new File(parentDir, child);
        String p = file4.getAbsolutePath();
        System.out.println(p);
    }
}
