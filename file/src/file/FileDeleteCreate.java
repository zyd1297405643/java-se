package file;

import org.junit.Test;

import java.io.File;
import java.io.IOException;


public class FileDeleteCreate {

    @Test
    public static void main(String[] args) throws IOException {

        // ⽂件的创建
        File f = new File("aaa.txt");
        System.out.println("是否存在:" + f.exists()); // false
        boolean newFile = f.createNewFile();// true
        System.out.println("是否存在:" + f.exists()); // true

        // ⽬录的创建
        File f2 = new File("newDir");
        System.out.println("是否存在:" + f2.exists()); // false
        System.out.println("是否创建:" + f2.mkdir()); // true
        System.out.println("是否存在:" + f2.exists()); // true
        // 创建多级⽬录
        File f3 = new File("newDira\\newDirb");
        System.out.println(f3.mkdir()); // false
        File f4 = new File("newDira\\newDirb");
        System.out.println(f4.mkdirs()); // true

       /* // ⽂件的删除
        System.out.println(f.delete()); // true
        // ⽬录的删除
        f3.delete();
        System.out.println(f2.delete()); // true
        System.out.println(f4.delete()); // false*/
    }

}

