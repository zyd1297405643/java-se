package bufferio;

import org.junit.Test;

import java.io.*;

public class BufferDemo {
    @Test
    public void test() throws FileNotFoundException {
        //1.缓冲流是用字节流来进行包装的
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("c.txt"));
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("c.txt"));
    }

    @Test
    public void test1() throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("bai.mp3"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("bufferDemo.mp3"));
        int len;
        long time1 = System.currentTimeMillis();
        while ((len = bis.read()) != -1) {
            //写入文件
            bos.write(len);

        }
        long time2 = System.currentTimeMillis();
        System.out.println(time2 - time1);


    }
    @Test
    public void test2() throws IOException {
        FileInputStream bis = new FileInputStream("bai.mp3");
        FileOutputStream bos = new FileOutputStream("bufferDemo.mp3");
        int len;
        long time1 = System.currentTimeMillis();
        while ((len = bis.read()) != -1) {
            //写入文件
            bos.write(len);

        }
        long time2 = System.currentTimeMillis();
        System.out.println(time2 - time1);


    }

    //字节流和 缓冲字节流   单个字节复制效率
    @Test
    public void test3() throws IOException {
        FileOutputStream fos = new FileOutputStream("bufferDemo-note.txt");
        FileInputStream fis = new FileInputStream("note.txt");
        int len;
        long time1=System.currentTimeMillis();
        while ((len= fis.read()) != -1) {
            //写入文件
            fos.write(len);

        }
        long time2=System.currentTimeMillis();
        System.out.println(time2-time1);//137
    }
    @Test
    public void test4() throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("note.txt"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("bufferDemo-note.txt"));
        int len;
        long time1 = System.currentTimeMillis();
        while ((len = bis.read()) != -1) {
            //写入文件
            bos.write(len);
        }
        bos.flush();
        long time2 = System.currentTimeMillis();
        System.out.println(time2 - time1);//3
    }


    //字节流和 缓冲字节流  使用字节数组复制效率
    @Test
    public void test5() throws IOException {
        FileOutputStream fos = new FileOutputStream("bufferDemo-note.txt");
        FileInputStream fis = new FileInputStream("note.txt");
        byte[] b=new byte[10];
        int len;
        long time1=System.currentTimeMillis();
        while ((len= fis.read(b)) != -1) {
            //写入文件
            fos.write(len);

        }
        long time2=System.currentTimeMillis();
        System.out.println(time2-time1);//18
    }

    @Test
    public void test6() throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("note.txt"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("bufferDemo-note.txt"));
        byte[] b=new byte[10];
        int len;
        long time1 = System.currentTimeMillis();
        while ((len = bis.read(b)) != -1) {
            //写入文件
            bos.write(len);
        }
        bos.flush();
        long time2 = System.currentTimeMillis();
        System.out.println(time2 - time1);//0
    }

}
