package wr;

import org.junit.Test;

import java.io.*;

public class Demo {
    @Test
    public void test() throws IOException {
        FileOutputStream fos = new FileOutputStream("wrDemo.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        /*osw.write(97);
        osw.write("你");
        osw.write('好');*/
        osw.write(-1);
        //不是底层流就要手动刷新或者关闭流
        osw.close();
    }

    @Test
    public void test2() throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream("wrDemo.txt"));
        //int 返回 32位
        //读字符- 16位，永远填不满32位，所以得到的永远是整数
        System.out.println(isr.read());//65535


    }

    @Test
    public void test3() throws IOException {
        FileOutputStream fos = new FileOutputStream("wrDemo.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        char[] ch = {'h', 'e', 'l', 'l', 'o'};
        //截取 llo
        //void write(char[] cbuf, int off, int len)
        //写入字符数组的某一部分。
        osw.write(ch, 2, 3);
        //void write(int c)
        osw.write(2);
        //void write(String str, int off, int len)
        //写入字符串
        osw.write("hello", 1, 4);
        osw.close();


    }
}
