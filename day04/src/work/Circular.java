package work;

public class Circular {

        double Radius;
        double Pi = 3.14;

        //构造器
        public Circular(double Radius) {
            this.Radius = Radius;
        }

        public  double area() {
            return Pi * Radius * Radius;
        }

        public double perimeter() {

            return 2 * Pi * Radius;
        }

    }

