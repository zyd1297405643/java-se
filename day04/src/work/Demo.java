package work;

//.定义一个类(圆形), 包含成员变量: 半径
//    构造器: 给半径初始化
//    方法: 1.用来求面积, 2.用来求周长
//
//    测试: 创建一个圆, 半径自己指定
//          打印圆的周长和面积
public class Demo {
    public static void main(String[] args) {

       Circular circular=new Circular(4);

        System.out.println("半径为"+circular.Radius +"圆形面积为："+circular.area());

        System.out.println("半径为"+circular.Radius +"圆形周长为："+circular.perimeter());

    }

}







