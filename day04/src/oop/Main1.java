package oop;
//面向对象小测试案例（主类：执行主方法）
public class Main1 {
    public static void main(String[] args) {
        //创建对象 new Person()->堆中    p->栈中
        //p只是一个引用类型的局部变量，指向堆中的对象
        Person person=new Person();
        System.out.println("person.age="+person.age);
        person.age=10;
        System.out.println("person.age="+person.age);
          person.sleep();
          Person person1=new Person();
          person1.age=18;
        System.out.println("person1.age="+person1.age);
    }

}
