package oop;
//构造方法测试
public class Main2 {
    public static void main(String[] args) {
        /*new Person的详解
        * 1.new 在堆中分配空间 ，并且将对象中的成员变量 赋默认值
        * Person() 调用构造器，成员变量初始化    */

        //调用Person(int newAge)
        Person person=new Person(10);
        System.out.println("person.age="+person.age);
        Person person1=new Person(20);
        System.out.println("person1.age="+person1.age);

        //调用Person(newAge,newName)
        Person person2=new Person(18,"zyd");

        //调用 Person(String name)
        Person person3=new Person("zyd");
        System.out.println("person3.name="+person3.name+",person3.age="+person3.age);
    }
}
