package oop;

//定义一个类，代表人
public class Person {
    //成员变量--直接声明在类中
    int age;//基本数据类型默认值0
    String name;//引用类型默认值null；

    //成员方法 不加static
    public void sleep() {
        int time = 10;//局部变量，必须初始化
        System.out.println("睡了,睡眠时间" + time + "分钟");
    }

    //构造方法-无参构造方法
    public Person() {

    }

    //有参构造方法
    public Person(int newAge) {
        age = newAge;
    }

    public Person(int newAge, String newName) {
        age = newAge;
        name = newName;
        System.out.println("age=" + age + ",name=" + name);
    }

    public Person(String name) {
   this(19);

        this.name=name;
    }
}
