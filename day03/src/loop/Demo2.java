package loop;

public class Demo2 {
    public static void main(String[] args) {
        /*continue  结束本轮循环，继续下次循环*/
        /*break     结束整个循环*/
        /*loop for()   break loop*/
        int i = 1;
        int j = 1;
            for (i = 1; i < 10; i++) {
            for (j = 1; j <=i; j++) {

                System.out.print(+i+"*"+j+"="+i*j+"\t");
            }
            System.out.println();
        }

    }
}
