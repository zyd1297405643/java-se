package loop;

import java.util.Scanner;

public class Demo4 {
    public static void main(String[] args) {
        double sqrt = Math.sqrt(9.0);
        System.out.println(sqrt);
        double abs = Math.abs(-10.0);
        System.out.println(abs);
        //向上取整
        double ceil = Math.ceil(-3.14);
        System.out.println(ceil);
        //向下取整
        double floor = Math.floor(-3.14);
        System.out.println(floor);
        //幂
        double pow = Math.pow(2.0, 3.0);
        System.out.println(pow);
        //四舍五入
        double round = Math.round(3.58);
        System.out.println(round);
        //系统时间
        long now = System.currentTimeMillis();
        System.out.println(now);
        //系统输入
        // 输入整行
        Scanner l = new Scanner(System.in);
        String line = l.nextLine();
        System.out.println("整行" + line);
        // 输入整数
        Scanner i = new Scanner(System.in);
        int int1 = i.nextInt();
        System.out.println("输出整数" + int1);
       //输入小数
        Scanner d = new Scanner(System.in);
        double dob = d.nextDouble();
        System.out.println("输出小数" + dob);
        //输入字符串
        Scanner s= new Scanner(System.in);
        String str=s.next();
        System.out.println("输出字符串"+str);
        String s1="hello";
        String s2="hello";
        if(s1.equals(s2))
            System.out.println(true);
    }

}
