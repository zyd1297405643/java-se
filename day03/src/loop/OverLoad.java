package loop;

public class OverLoad {
    //函数重载
    public static void main(String[] args) {
        int a = 1, b = 2;
        char c='q';
        print();
        print(c);
        print(a);
        print(a + b);
    }

    public static void print() {
        System.out.println();
    }

    public static void print(char a) {
        System.out.println("char 类参数型"+a);
    }

    public static void print(int a) {
        System.out.println("int 类型参数"+a);
    }

    public static void print(int a, int b) {
        System.out.println("int类型参数"+a + b);
    }
}
