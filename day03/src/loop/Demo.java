package loop;

public class Demo {

    public static void main(String[] args) {
        int i = 10;
        while (i > 0) {
            i--;
            System.out.println(i + " ");
        }
        do {
            System.out.println(i + " ");
        } while (i > 0);
    }
}
