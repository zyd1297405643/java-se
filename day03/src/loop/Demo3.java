package loop;

public class Demo3 {
    /*方法名 首字母小写，后面驼峰命名法*/
    /*返回值类型*/
   /* a 没有返回值，类型就是void ，return 可以出现 但是return 后面的不可以有值，只表表示方法结束
     b. 有返回值 ，类型是8种数据类型，return 对应的返回值得是对应的数据类型*/
     /* 参数 */
    /*方法里的是形参，主函数调用时时实参*/
    public static void main(String[] args) {
        int answer=method(1, 2);
        System.out.println(answer);
    }

    public static int method(int x, int y) {
        int a, b;
        a = x;
        b = y;
        return a+b;
    }

}
