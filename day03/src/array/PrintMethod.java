package array;

public class PrintMethod {
    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 4, 5};
        int[] b = new int[]{9, 32, 21, 1, 3};
        print(a);
        System.out.println();
        print(b);
    }

    //打印的方法
    public static void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
