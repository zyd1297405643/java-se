package array;
//双色球
/* 双色球: 红球 1~32, 随机生成5个, 不能重复
            存在数组中, 并且红球先由小到大排好序
            再在数组中, 额外添加一个蓝色球, 范围1~16*/
import java.util.Arrays;

public class BichromaticSphere {
    public static void main(String[] args) {
        int[] balls = new int[5];
        for (int i = 0; i < balls.length; i++) {
            balls[i] = (int) (Math.random() * 5) + 1;
            for (int j = 0; j < i; j++) {
                if (balls[i] == balls[j]) {
                    i--;
                    break;
                }

            }

        }
        Arrays.sort(balls);
        System.out.println("输出从小到大排序后的红色球:");
        System.out.println(Arrays.toString(balls));
        //扩容+复制
        balls = Arrays.copyOf(balls, balls.length + 1);
        //添加最后一个球
        balls[balls.length - 1] = (int) (Math.random() * 16) + 1;
        System.out.println("输出添加蓝球后双色球:");
        System.out.println(Arrays.toString(balls));
    }

}
