package array;

import java.util.Arrays;

public class ArrayscopyOf {
    public static void main(String[] args) {
       /* int[] src = new int[]{1, 2, 3, 4, 5};
        //—>src[1,2,3,4,5,6]
        int[] NewArray = new int[src.length+1];
        //复制
        for (int i = 0; i < src.length; i++) {
            NewArray[i]=src[i];
        }
        //扩容
        NewArray[NewArray.length-1]=6;
        //src重新指向NewArray
        src=NewArray;
         System.out.println(Arrays.toString(src));
*/
//       使用数组方法的方式来扩容
        int[] src = new int[]{1,3,2,4,5};
//        扩容+复制+赋值（src重新指向）
//        Arrays.copyOf(src,src.length+1) 需要扩容的数组和需要扩容的长度
        src= Arrays.copyOf(src,src.length+1);
        src[src.length-1]=6;
//        Arrays.toString(src) 快速打印
        System.out.println(Arrays.toString(src));
        // Arrays.sort(src) 从小到大排序
        Arrays.sort(src);
        System.out.println(Arrays.toString(src));
    }
}
