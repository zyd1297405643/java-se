package array;

public class Array {
    public static void main(String[] args) {
        int max, min;
        max = 0;
        min = 99;
        int[] array = new int[10];
        int[] a = {1, 2, 3, 4};
        // a={5,4,3,2,1}错误用法
        a = new int[]{1, 2, 3, 4, 7};//重新定义
        System.out.println("长度" + a.length);
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + "\t");
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            array[i] = (int) (Math.random() * 100);
            System.out.print(+array[i] + "\t");
            if (array[i] >= max) {
                max = array[i];
            } else if (array[i] <= min)
                min = array[i];
        }
        System.out.println();
        System.out.println("最大值" + max);
        System.out.println("最小值" + min);
        System.out.println(array);
        /*[表示数组  I表示数据类型 @固定字符  3b6eb2ec表示第0个元元素的地址*/
    }
}
