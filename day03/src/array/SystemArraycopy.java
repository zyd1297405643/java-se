package array;

import java.util.Arrays;

public class SystemArraycopy {
    public static void main(String[] args) {
        int i, j;
        int[] array = new int[]{1, 2, 3, 4, 5, 7};
        int[] newArray=new int[7];
        //System.arraycopy(原数组，原数组下标，新数组，新数组开始复制的下标，复制的长度);
        System.arraycopy(array,1,newArray,2,3);
        System.out.println(Arrays.toString(newArray));
    }
}
