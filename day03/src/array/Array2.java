package array;

public class Array2 {
    public static void main(String[] args) {
        int[] Array = new int[]{1, 2, 3, 4, 5};
        System.out.println("输出数组1");
        print1(Array);
        System.out.println("输出数组第0个元素");
        System.out.println(Array[0]+" ");
        //a[0]=7;
        System.out.println("输出数组2");
        print2(Array);
        System.out.println("输出数组第0个元素");
        System.out.println(Array[0]+" ");
    }
    public static void print1(int[] array) {
         array[0]=7;
        for (int i = 0; i <array.length ; i++) {
            System.out.print(array[i]+" ");
        }
        System.out.println();
    }
    public static void print2(int[] array) {
        array=new int[]{3,4,6,8};
        for (int i = 0; i <array.length ; i++) {
            System.out.print(array[i]+" ");

        }
        System.out.println();
        System.out.println("重新new后的数组的第0个元素："+array[0]);
    }
}
