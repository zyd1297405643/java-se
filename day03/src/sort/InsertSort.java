package sort;

import java.util.Arrays;

//插入排序
/*•	temp 代表取出待插入的元素
•	i 代表后组待插入元素的位置
•	j 代表前组每个元素的位置
*/
public class InsertSort {
    public static void main(String[] args) {
        int i, j, temp;
        int[] ary = new int[]{8, 2, 3, 7, 1};
        for (i = 1; i < ary.length; i++) {
            temp = ary[i];
//            利用循环查找插入位置：到头j=-1
            for (j = i - 1; j >= 0; j--) {
                if (temp < ary[j]) {
                    ary[j + 1] = ary[j];
                    //[j]->[j+1]向后移动
                }
//                temp不小于ary[j]
                else break;

            }
            ary[j + 1] = temp;//插入temp->[j+1]

        }
        System.out.println(Arrays.toString(ary));

    }
}
