package sort;

import java.util.Arrays;

//选择排序/*•	将数组中每个元素与第一个元素比较，
// 如果这个元素小于第一个元素，则交换这两个元素
//•	循环第 1 条规则，找出最小元素，放于第 1 个位置
//•	经过 n-1 轮比较完成排序 */
/*public class SelectSort {
    public static void main(String[] args) {
        int[] array = new int[]{8, 2, 3, 7, 1};
//       	i 代表第一个数据的位置   j 代表i后面的每一个数据的位置
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
              *//*  System.out.print(Arrays.toString(array));
                System.out.print(i+"\t");
                System.out.print(j+"\t");
                System.out.print(array[i]+"\t");
                System.out.print(array[j]+"\t");
                System.out.print((array[i]>array[j])+"\t");*//*
                if (array[i] > array[j]) {
                  *//*  System.out.print(array[i]+"<->"+array[j]);*//*
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
               *//* System.out.println();*//*
            }
            System.out.println(Arrays.toString(array));
        }

    }
}*/
public class SelectSort {
    public static void main(String[] args) {
        int[] array = new int[]{ 2, 3, 6, 4, 0, 1, 7, 8, 5, 9 };
//       	i 代表第一个数据的位置   j 代表i后面的每一个数据的位置
        for (int i = 0; i < array.length - 1; i++) {
            for(int j=i+1;j<array.length;j++){
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
            System.out.println(Arrays.toString(array));
        }
    }


}
