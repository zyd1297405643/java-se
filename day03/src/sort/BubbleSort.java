package sort;

import java.util.Arrays;

//冒泡排序比较相邻的元素，将小的放到前面。
/*•	i 代表次数
•	j 代表比较位置
*/
/*public class BubbleSort {
    public static void main(String[] args) {
    int[] array=new int[]{3,2,1,5,4};
        for (int i = 0; i <array.length -1; i++) {
            for (int j = 0; j < array.length-i-1; j++) {
            System.out.print(Arrays.toString(array)+"\t");
                System.out.print("i="+i+"\t");
                System.out.print("j="+j+"\t");
                System.out.print("array["+j+"]=" +array[j]+"\t");
                System.out.print("array["+(j+1)+"]="+array[j+1]+"\t");
                System.out.print(("array["+j+"]>array["+(j+1)+"]=")+(array[j]>array[j+1])+"\t");
                 if (array[j]>array[j+1])
                 {
                     System.out.println(array[j]+"<->"+array[j+1]);
                     int temp=array[j];
                     array[j]=array[j+1];
                     array[j+1]=temp;
                 }
                System.out.println();
            }


        }

    }


}*/
public class BubbleSort {
    public static void main(String[] args) {
        int[] array=new int[]{ 2, 3, 6, 4, 0, 1, 7, 8, 5, 9};
        for (int i = 0; i < array.length-1; i++) {
            for (int j =0; j <array.length -1-i; j++) {
                if (array[j]>array[j+1]){
                    int temp=array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;
                }
            }
            //   每次循环 遍历一次数组看结果
            System.out.println(Arrays.toString(array));
        }

    }
}

